/*
 Xinjie Lei
 lei13@purdue.edu
 
 forward unit interface
 */
`ifndef HAZARD_UNIT_IF_VH
`define HAZARD_UNIT_IF_VH

//all types
 `include "cpu_types_pkg.vh"
interface hazard_unit_if;
   import cpu_types_pkg::*;
   
   logic [4:0] idexRt, ifidRs, ifidRt;
   logic idexdmemREN,idexdmemWEN, idexAtomic, exmemdmemREN, exmemdmemWEN, ihit, dhit, Halt,pcWEN, ifidWEN, idexWEN, exmemWEN, memwbWEN, ifidFlush, idexFlush, exmemFlush, Over, iscorrect, isjump, isbranch; 

   //hazard unit ports
   modport hz( 
   	       input idexRt, ifidRt, ifidRs, ihit, dhit, idexdmemREN, idexdmemWEN, idexAtomic, exmemdmemREN, exmemdmemWEN, Halt, Over, iscorrect, isjump, isbranch,
	       output pcWEN, ifidWEN, idexWEN, exmemWEN, memwbWEN, ifidFlush, idexFlush, exmemFlush
	       );
   modport hztb(
		input pcWEN, ifidWEN, idexWEN, exmemWEN, memwbWEN, ifidFlush, idexFlush, exmemFlush,
		output idexRt, ifidRt, ifidRs, ihit, dhit, idexdmemREN, idexdmemWEN, idexAtomic, exmemdmemREN, exmemdmemWEN, Halt, Over, iscorrect, isjump, isbranch
		);
   
  
endinterface

`endif //HAZARD_UNIT_IF_VH
