/*
 Xinjie Lei
 lei13@purdue.edu
 
 MEM/WB register interface
 */
`ifndef MEMWB_IF_VH
`define MEMWB_IF_VH

//all types
 `include "cpu_types_pkg.vh"
interface memwb_if;
   import cpu_types_pkg::*;
   
   word_t npc, result, dmemload;
   logic [4:0] Rd;
   logic [1:0] RegSrc;
   logic WEN, RegWEN, Halt, Over; 

   word_t npc_in, result_in, dmemload_in;
   logic [4:0] Rd_in;
   logic [1:0] RegSrc_in;
   logic RegWEN_in, Halt_in, Over_in; 

   word_t memwbinstru, memwbinstru_in;
   //MEMWB register ports
   modport memwb( 
   	       input WEN,npc_in,result_in, Rd_in, dmemload_in, RegWEN_in, RegSrc_in, Halt_in, Over_in, memwbinstru_in,
	       output npc, result, Rd, dmemload, RegWEN, RegSrc, Halt, Over, memwbinstru
	       );
  
endinterface

`endif //MEMWB_IF_VH
