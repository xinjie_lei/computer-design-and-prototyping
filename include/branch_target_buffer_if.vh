/*
 Xinjie Lei
 lei13@purdue.edu
 
 branch target buffer interface
 */
`ifndef BTB_IF_VH
`define BTB_IF_VH

//all types
`include "cpu_types_pkg.vh"
interface branch_target_buffer_if;
   import cpu_types_pkg::*;
   
   word_t pc, bta, taddr, baddr; 
   logic [1:0] ridx, widx;
   logic br, hit, ishit, istaken, iscorrect, isbranch, WEN;   
   //branch target buffer ports
   modport btb( 
		input pc, bta, baddr, istaken, ishit, isbranch, ridx, widx,	
		output br, taddr, hit
	       );
   modport btb_tb( 
	   	input br, taddr, hit,
		output pc, bta, baddr, istaken, ishit, isbranch, ridx, widx	
	       );
  
endinterface

`endif //BTB_IF_VH
