/*
 Xinjie Lei
 lei13@purdue.edu
 
 EX/MEM register interface
 */
`ifndef EXMEM_IF_VH
`define EXMEM_IF_VH

//all types
 `include "cpu_types_pkg.vh"
interface exmem_if;
   import cpu_types_pkg::*;
   
   word_t npc, result, dmemstore, rdat1;
   logic [4:0] Rd;
   logic [1:0] RegSrc;
   logic WEN, Flush, dmemREN, dmemWEN, RegWEN, Halt, Over, Atomic; 

   word_t npc_in, result_in, dmemstore_in, rdat1_in;
   logic [4:0] Rd_in;
   logic [1:0] RegSrc_in;
   logic dmemREN_in, dmemWEN_in, RegWEN_in, Halt_in, Over_in, Atomic_in; 
   
   word_t exmeminstru, exmeminstru_in;
   //EXMEM register ports
   modport exmem( 
   	       input WEN, Flush, npc_in,result_in, Rd_in, dmemstore_in, dmemREN_in, dmemWEN_in, RegWEN_in, RegSrc_in, Halt_in, Over_in, rdat1_in, Atomic_in, exmeminstru_in,
	       output npc, result, Rd, dmemstore,  dmemREN, dmemWEN, RegWEN, RegSrc, Halt, Over, rdat1, Atomic, exmeminstru
	       );
  
endinterface

`endif //EXMEM_IF_VH
