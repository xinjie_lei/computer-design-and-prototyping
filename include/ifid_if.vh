/*
 Xinjie Lei
 lei13@purdue.edu
 
 IF/ID register interface
 */
`ifndef IFID_IF_VH
`define IFID_IF_VH

//all types
 `include "cpu_types_pkg.vh"
interface ifid_if;
   import cpu_types_pkg::*;
   
   word_t instruc_in, instruc, npc, npc_in, baddr, baddr_in;
   logic WEN, Flush, isbr_in, isbr, ishit_in, ishit;
   // IFID register ports
   modport ifid (
   	       	input instruc_in, npc_in, WEN, Flush, isbr_in, ishit_in, baddr_in,
	       	output npc, instruc, isbr, ishit, baddr
	       	);
  
endinterface

`endif //IFID_IF_VH