/*
 Xinjie Lei
 lei13@purdue.edu
 
 IF/ID register interface
 */
`ifndef IFID_IF_VH
`define IFID_IF_VH

//all types
 `include "cpu_types_pkg.vh"
interface ifid_if;
   import cpu_types_pkg::*;
   
   word_t instruc, npc, npc_in, baddr, baddr_in;
   logic [5:0] InstruOp,  FuncOp;
   logic [25:0] JmpImm;
   logic [15:0] Imm;
   logic [4:0] Rs, Rt, Rd, Shamt;
   logic WEN, Flush, isbr_in, isbr, ishit_in, ishit;
   //word_t ifidinstru;
   // IFID register ports
   modport ifid (
   	       	input instruc, npc_in, WEN, Flush, isbr_in, ishit_in, baddr_in,
	       	output npc, InstruOp, FuncOp, Rs, Rt, Rd, Imm, JmpImm, Shamt, isbr, ishit, baddr
	       	);
  
endinterface

`endif //IFID_IF_VH