/*
 Xinjie Lei
 lei13@purdue.edu
 
 request unit interface
 */
`ifndef REQUEST_UNIT_IF_VH
`define REQUEST_UNIT_IF_VH

interface request_unit_if;

   logic memREN, memWEN, ihit, dhit, imemREN, dmemREN, dmemWEN, halt;

   // request unit ports
   modport ru (
	       input  memREN, memWEN, ihit, dhit, halt,
	       output imemREN, dmemREN, dmemWEN
	       );
   // request unit tb
   modport rutb (
		 input imemREN, dmemREN, dmemWEN,
		 output memREN, memWEN, ihit, dhit, halt
		 );
endinterface

`endif //REQUEST_UNIT_IF_VH
