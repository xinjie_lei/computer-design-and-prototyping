`ifndef ALU_IF_VH
`define ALU_IF_VH

// all types
`include "cpu_types_pkg.vh"

interface alu_if;
  // import types
  import cpu_types_pkg::*;

  aluop_t op;
  logic over, zero, nega;
  word_t    ina, inb, result;

  // alu ports
  modport alu (
    input  ina, inb, op,
    output  result, over, zero, nega
  );
  // alu tb ports
  modport alutb(
    output ina, inb, op,
    input  result, over, zero, nega
  );
endinterface

`endif //ALU_IF_VH
