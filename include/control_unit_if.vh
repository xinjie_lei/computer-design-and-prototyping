/*
 Xinjie Lei
 lei13@purdue.edu
 
 control unit interface
 */
`ifndef CONTROL_UNIT_IF_VH
 `define CONTROL_UNIT_IF_VH

//all types
 `include "cpu_types_pkg.vh"
interface control_unit_if;
   import cpu_types_pkg::*;

   logic Halt, Beq, Bne, Jump, Zero, RegWEN, Jr, memREN, memWEN, ExtOp, InaSrc, nedOver, Atomic;
   logic [1:0] RegDst, InbSrc, RegSrc;
   aluop_t ALUOp;
   logic [5:0] FuncOp;
   logic [5:0] InstruOp;

   // control unit ports
   modport cu (
   	       input  InstruOp, FuncOp, Zero,
	       output Halt, Beq, Bne, Jump, RegSrc, RegWEN, Jr, memREN, memWEN,RegDst, ExtOp, InaSrc, InbSrc, ALUOp, nedOver, Atomic
	       );
   // control unit tb
   modport cutb (
   	   	 input 	Halt, Beq, Bne, Jump, RegSrc, RegWEN, Jr, memREN, memWEN,RegDst, ExtOp, InaSrc, InbSrc, ALUOp, nedOver, Atomic,
 		 output InstruOp, FuncOp, Zero
		 );
endinterface

`endif //CONTROL_UNIT_IF_VH
