/*
 Xinjie Lei
 lei13@purdue.edu
 
 ID/EX register interface
 */
`ifndef IDEX_IF_VH
`define IDEX_IF_VH

//all types
 `include "cpu_types_pkg.vh"
interface idex_if;
   import cpu_types_pkg::*;
   
   word_t npc, rdat1, rdat2, ExtImm, baddr;
   logic [4:0] Rs, Rt, Rd, Shamt;
   logic [1:0] RegSrc, RegDst, InbSrc;
   logic Beq, Bne, dmemREN, dmemWEN, RegWEN, InaSrc, Halt, isbr, ishit, nedOver, Atomic; 
   aluop_t ALUOp;

   word_t npc_in, rdat1_in, rdat2_in, ExtImm_in, baddr_in;
   logic [4:0] Rs_in, Rt_in, Rd_in, Shamt_in;
   logic [1:0] RegSrc_in, RegDst_in, InbSrc_in;
   logic Beq_in, Bne_in, dmemREN_in, dmemWEN_in, RegWEN_in, InaSrc_in, Halt_in, WEN, Flush, isbr_in, ishit_in, nedOver_in, Atomic_in; 
   aluop_t ALUOp_in;
   word_t idexinstru, idexinstru_in;
   //IDEX register ports
   modport idex( 
   	       input WEN, Flush, npc_in, rdat1_in, rdat2_in, ExtImm_in, Rs_in, Rt_in, Rd_in, Shamt_in, Beq_in, Bne_in, dmemREN_in, dmemWEN_in, RegWEN_in, RegDst_in, ALUOp_in, InaSrc_in, InbSrc_in, RegSrc_in, Halt_in, isbr_in, ishit_in, baddr_in, nedOver_in, Atomic_in, idexinstru_in,
	       output npc, rdat1, rdat2, ExtImm, Rs, Rt, Rd, Shamt, Beq, Bne, dmemREN, dmemWEN, RegWEN, RegDst, ALUOp, InaSrc, InbSrc, RegSrc, Halt, isbr, ishit, baddr, nedOver, Atomic, idexinstru
	       );
  
endinterface

`endif //IDEX_IF_VH