#----------------------------------------------------------
# First Processor
#----------------------------------------------------------
# s0: lock1
# s1: 256
# s2: size
# s3: head addr
# s4: buttom addr
# s5: 10
	
  org   0x0000              # first processor p0
  ori   $sp, $zero, 0x3ffc  # stack
  jal   mainp0              # go to program
  halt
		
# main function 0
mainp0:
  push  $ra                 # save return address
  ori   $s0, $zero, lock1
  ori   $s1, $zero, 256
  ori   $s2, $zero, size
  ori   $s3, $zero, fifo       
  addiu $s4, $s3, 40
  ori   $s5, $zero, 10
  ori   $v0, $zero, 8       #seed

start_0: 
#check full
  lw    $t8, 0($s2)         #t8 = size
  beq   $t8, $s5, start_0
	
crc:	
# v0 = previous random number
  or    $a0, $zero, $v0     #initialize a0 for crc with v0
  jal   crc32
	
# critical code segment
  jal   lock
  sw    $v0, 0($s3)         #store new crc v0 to fifo
  lw    $t8, 0($s2)         #synchronize in size
  addiu $t8, $t8, 1         #increase size by 1
  sw    $t8, 0($s2)
  jal   unlock              # release the lock

#update head addr  		
  addiu $s3, $s3, 4         #new head = head + 4
  beq   $s3, $s4, set
#update counter
updcnt_0:
  ori   $t8, $zero, 1
  subu  $s1, $s1, $t8
  bne   $s1, $zero, start_0
	
#exit	
  pop   $ra
  jr    $ra
	
set:
  ori   $s3, $zero, fifo
  j     updcnt_0
	
#----------------------------------------------------------
# Second Processor
#----------------------------------------------------------
# s0: lock1
# s1: 256
# s2: size
# s3: tail addr
# s4: buttom
# s5: sum
# t7, current random number
# t8, minval
# t9, maxval
	
  org   0x200               # second processor p1
  ori   $sp, $zero, 0x7ffc  # stack
  jal   mainp1              # go to program
  halt 
# main function 1
mainp1:
  push  $ra                 # save return address
  ori   $s0, $zero, lock1
  ori   $s1, $zero, 256
  ori   $s2, $zero, size
  ori   $s3, $zero, fifo
  addiu $s4, $s3, 40
  ori   $s5, $zero, 0
	
start_1: 	
#check empty
  lw    $t4, 0($s2)
  beq   $t4, $zero, start_1   #fifo empty, go to release
	
#fifo not empty: consume one number and set new tail offset
  lw    $t7, 0($s3)         #load current random number
  andi  $t7, $t7, 0xFFFF
  addu  $s5, $s5, $t7       #s5 has new sum
#get min and max	
  ori   $t1, $zero, minval
  lw    $a0, 0($t1)
  or    $a1, $zero, $t7
  jal   min
  or    $t8, $v0, $zero    #t8, minval
	
  ori   $t2, $zero, maxval
  lw    $a0, 0($t2)	
  or    $a1, $zero, $t7
  jal   max
  or    $t9, $v0, $zero    #t9, maxval

# critical code segment
  jal   lock
  sw    $t8, 0($t1)
  sw    $t9, 0($t2)
  ori   $t3, $zero, 1
  lw    $t4, 0($s2)        #synchronize in size
  subu  $t4, $t4, $t3      #t4, size
  sw    $t4, 0($s2)
  jal   unlock

#update tail addr
  addiu $s3, $s3, 4
  beq   $s3, $s4, set1
	
#update counter
updcnt_1:
  subu  $s1, $s1, $t3
  bne   $s1, $zero, start_1
	
  jal   lock
  srl 	$s5, $s5, 8
  ori   $t0, $zero, average
  sw    $s5, 0($t0)
  jal   unlock
	
  pop   $ra
  jr    $ra

set1:
  ori  $s3, $zero, fifo
  j    updcnt_1
	
##################################################################
#                          lock		                 	 #
##################################################################
# pass in an address to lock function in argument register 0
# returns when lock is available
lock:
aquire:
  ll    $t0, 0($s0)         # load lock location
  bne   $t0, $0, aquire     # wait on lock to be open
  addiu $t0, $t0, 1
  sc    $t0, 0($s0)
  beq   $t0, $0, lock       # if sc failed retry
  jr    $ra

# pass in an address to unlock function in argument register 0
# returns when lock is free
unlock:
  sw    $0, 0($s0)
  jr    $ra


##################################################################
#                 subroutine_crc                                 #
##################################################################
#REGISTERS
#at $1 at
#v $2-3 function returns
#a $4-7 function args
#t $8-15 temps
#s $16-23 saved temps (callee preserved)
#t $24-25 temps
#k $26-27 kernel
#gp $28 gp (callee preserved)
#sp $29 sp (callee preserved)
#fp $30 fp (callee preserved)
#ra $31 return address

# USAGE random0 = crc(seed), random1 = crc(random0)
#       randomN = crc(randomN-1)
#------------------------------------------------------
# $v0 = crc32($a0)
crc32:
  lui $t1, 0x04C1
  ori $t1, $t1, 0x1DB7
  or  $t2, $0, $0
  ori $t3, $0, 32

l1:
  slt $t4, $t2, $t3
  beq $t4, $zero, l2

  srl $t4, $a0, 31
  sll $a0, $a0, 1
  beq $t4, $0, l3
  xor $a0, $a0, $t1
l3:
  addiu $t2, $t2, 1
  j l1
l2:
  or $v0, $a0, $0
  jr $ra
#------------------------------------------------------
#######################################################
#            subroutine_mm                            #                
#######################################################
# registers a0-1,v0,t0
# a0 = a
# a1 = b
# v0 = result

#-max (a0=a,a1=b) returns v0=max(a,b)--------------
max:
  push  $ra
  push  $a0
  push  $a1
  or    $v0, $0, $a0
  slt   $t0, $a0, $a1
  beq   $t0, $0, maxrtn
  or    $v0, $0, $a1
maxrtn:
  pop   $a1
  pop   $a0
  pop   $ra
  jr    $ra
#--------------------------------------------------

#-min (a0=a,a1=b) returns v0=min(a,b)--------------
min:
  push  $ra
  push  $a0
  push  $a1
  or    $v0, $0, $a0
  slt   $t0, $a1, $a0
  beq   $t0, $0, minrtn
  or    $v0, $0, $a1
minrtn:
  pop   $a1
  pop   $a0
  pop   $ra
  jr    $ra
#--------------------------------------------------

  org   0x4000 
lock1:	
  cfw   0x0

fifo:
  cfw   0x0
  cfw   0x0
  cfw   0x0
  cfw   0x0
  cfw   0x0
  cfw   0x0
  cfw   0x0
  cfw   0x0
  cfw   0x0
  cfw   0x0
	
size:
  cfw 0x0
	
average:
  cfw 0x0

minval:
  cfw 0xFFFF
	
maxval:
  cfw 0x0
 
               
