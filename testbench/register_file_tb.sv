/*
  Eric Villasenor
  evillase@gmail.com

  register file test bench
*/

// mapped needs this
`include "register_file_if.vh"

// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 1 ns

module register_file_tb;

  parameter PERIOD = 10;

  logic CLK = 0, nRST;

  // test vars
  int v1 = 1;
  int v2 = 4721;
  int v3 = 25119;

  // clock
  always #(PERIOD/2) CLK++;

  // interface
  register_file_if rfif ();
  // test program
  test PROG (CLK, nRST, rfif.tb);
  // DUT
`ifndef MAPPED
  register_file DUT(CLK, nRST, rfif);
`else
  register_file DUT(
    .\rfif.rdat2 (rfif.rdat2),
    .\rfif.rdat1 (rfif.rdat1),
    .\rfif.wdat (rfif.wdat),
    .\rfif.rsel2 (rfif.rsel2),
    .\rfif.rsel1 (rfif.rsel1),
    .\rfif.wsel (rfif.wsel),
    .\rfif.WEN (rfif.WEN),
    .\nRST (nRST),
    .\CLK (CLK)
  );
`endif

endmodule

program test(
	     input logic CLK,
	     output logic nRST,
	     register_file_if.tb tbif
	     );
   parameter PERIOD = 10;
   initial begin
      $monitor("@%00g CLK = %b nRST = %b rdat1 = %h rdat2 = %h wdat = %h rsel1 = %h rsel2= %h wsel = %h WEN = %b",$time, CLK, nRST, tbif.rdat1,tbif.rdat2, tbif.wdat, tbif.rsel1, tbif.rsel2, tbif.wsel, tbif.WEN);
      // Initialize all of the test inputs
      nRST = 1'b1; // Initialize to be inactive
	
      // Power-on Reset of the DUT
      #(0.1);
      nRST = 1'b0; // Need to actually toggle this in order for it to actually run dependent always blocks
      #(PERIOD); // Release the reset away from a clock edge
      nRST = 1'b1; // Deactivate the chip reset
	
      // Wait for a while to see normal operation
      #(PERIOD);

      //Test 1: Check if write/read reg 0 correct
      @(negedge CLK);
      tbif.WEN = 1'b1;
      tbif.wsel = 32'h0;
      tbif.wdat = 32'h19547AC6;
      #(PERIOD);
      tbif.WEN = 1'b0;
      tbif.rsel1 = 32'h0;
      tbif.rsel2 = 32'h0;
      #(PERIOD);

      //Test 2: Check if write/read reg 1 correct
      @(negedge CLK);
      tbif.WEN = 1'b1;
      tbif.wsel = 32'h1;
      tbif.wdat = 32'h19547AC6;
      #(PERIOD);
      tbif.WEN = 1'b0;
      tbif.rsel1 = 32'h1;
      tbif.rsel2 = 32'h0;
      #(PERIOD);

      //Test 3: Check if write/read readbus2 correct
      @(negedge CLK);
      tbif.WEN = 1'b1;
      tbif.wsel = 32'h2;
      tbif.wdat = 32'h19547AC6;
      #(PERIOD);
      tbif.WEN = 1'b0;
      tbif.rsel1 = 32'h0;
      tbif.rsel2 = 32'h2;
      #(PERIOD);

       //Test 4: Check if write/read readbus1 and readbus2 correct
      @(negedge CLK);
      tbif.WEN = 1'b1;
      tbif.wsel = 32'h2;
      tbif.wdat = 32'hBE67DA12;
      #(PERIOD);
      tbif.WEN = 1'b0;
      tbif.rsel1 = 32'h2;
      tbif.rsel2 = 32'h2;
      #(PERIOD);
      
      //Test 5: Check if write/read readbus1 and readbus2 from different reg correct
      @(negedge CLK);
      tbif.WEN = 1'b1;
      tbif.wsel = 32'h1;
      tbif.wdat = 32'h3451FDEA;
      #(PERIOD);
      tbif.WEN = 1'b0;
      tbif.rsel1 = 32'h1;
      tbif.rsel2 = 32'h2;
      #(PERIOD);
   end
endprogram
