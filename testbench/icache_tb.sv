// interface
`include "cache_control_if.vh"
`include "datapath_cache_if.vh"

// types
`include "cpu_types_pkg.vh"

// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 1 ns

module icache_tb;
   // clock period
   parameter PERIOD = 10;

   // signals
   logic CLK = 1, nRST;

   // clock
   always #(PERIOD/2) CLK++;

   // interface
   cache_control_if ccif();
   datapath_cache_if dpif();
   
   // test program
   test PROG (CLK, nRST, dpif, ccif);

   // dut
   icache ICACHE (CLK, nRST, dpif, ccif);
   
endmodule // icache_tb

program test
    import cpu_types_pkg::*;
   (
    input logic  CLK,
    output logic nRST,
    datapath_cache_if.dp dpif,
    cache_control_if.cc ccif
    );
   parameter PERIOD=10;
   initial begin
      //reset chip
      nRST = 1'b1;
      #(PERIOD);
      nRST = 1'b0;
      #(PERIOD);
      nRST = 1'b1;

      //test1: dpif.imemREN = 0
      ccif.iwait = 1'b1;

      dpif.imemREN = 1'b0;
      dpif.imemaddr = 32'h4;
      #(PERIOD);
      
      //test2: miss
      dpif.imemREN = 1'b1;
      dpif.imemaddr = 32'h0;
      #(PERIOD);
      ccif.iload = 32'hda1da1da;
      #(PERIOD);
      ccif.iwait = 1'b0;
      
      //test3: hit 
      #(PERIOD);   
      dpif.imemREN = 1'b1;
      dpif.imemaddr = 32'h0;
      #(PERIOD);
      
   end // initial begin
endprogram
     