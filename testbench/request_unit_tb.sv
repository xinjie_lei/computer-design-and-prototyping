/*
  Xinjie Lei
  lei13@purdue.edu

  request unit test bench
*/

// mapped timing needs this. 1ns is too fast
`include "request_unit_if.vh"
`timescale 1 ns / 100 ps

module request_unit_tb;

  parameter PERIOD = 10;

  logic CLK = 0, nRST;

  // clock
  always #(PERIOD/2) CLK++;
  //interface
  request_unit_if ruif();
  // test program
  test PROG (CLK, nRST, ruif.rutb);
  // DUT
  request_unit DUT(CLK, nRST, ruif.ru);


endmodule

program test(
  input logic CLK,
  output logic nRST,
  request_unit_if.rutb tbif
	);
  parameter PERIOD = 10;
  initial begin
    //Reset request unit
    nRST = 1'b1;
    nRST = 1'b0;
    #(PERIOD);
    nRST = 1'b1;
    //Test 1:instruction read
    tbif.memREN = 1'b0;
    tbif.memWEN = 1'b0;
    tbif.ihit = 1'b0;
    tbif.dhit = 1'b0;
    #(2*PERIOD);
    tbif.ihit = 1'b1;
    #(PERIOD);
     //Test 2:data read
    tbif.memREN = 1'b1;
    tbif.memWEN = 1'b0;
    tbif.ihit = 1'b0;
    tbif.dhit = 1'b0;
    #(2*PERIOD);
    tbif.dhit = 1'b1;
    #(PERIOD);
    //Test 3:data write
    tbif.memREN = 1'b0;
    tbif.memWEN = 1'b1;
    tbif.ihit = 1'b0;
    tbif.dhit = 1'b0;
    #(2*PERIOD);
    tbif.dhit = 1'b1;
    #(PERIOD);

  end
endprogram