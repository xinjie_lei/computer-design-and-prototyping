/*
 Xinjie Lei
 lei13@purdue.edu
 branch target buffer test bench
 */

// mapped needs this
`include "branch_target_buffer_if.vh"

// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 1 ns

module branch_target_buffer_tb;

   parameter PERIOD = 10;

   logic CLK = 0, nRST;

   // clock
   always #(PERIOD/2) CLK++;

   // interface
   branch_target_buffer_if btbif ();
   // test program
   test PROG (CLK, nRST, btbif);
   // DUT
   branch_target_buffer DUT(CLK, nRST, btbif);


endmodule

program test(
	     input logic CLK,
	     output logic nRST,
	     branch_target_buffer_if.btb_tb btbif
	     );
   parameter PERIOD = 10;
   initial begin
      // Initialize all of the test inputs
      nRST = 1'b1; // Initialize to be inactive
      
      // Power-on Reset of the DUT
      #(0.1);
      nRST = 1'b0; // Need to actually toggle this in order for it to actually run dependent always blocks
      #(PERIOD); // Release the reset away from a clock edge
      nRST = 1'b1; // Deactivate the chip reset
      
      // Wait for a while to see normal operation
      #(PERIOD);

      //Test 1: update btb to fullload 
      @(posedge CLK);
      btbif.pc = 32'h09;
      btbif.ridx = btbif.pc[3:2];     
      #(PERIOD*2);
      btbif.baddr = btbif.pc;
      btbif.widx = btbif.ridx;
      btbif.istaken = 1'b1;
      btbif.bta = 32'h4C;
      btbif.pc = 32'h1F;
      btbif.ridx = btbif.pc[3:2];
      #(PERIOD*2);
      btbif.baddr = btbif.pc;
      btbif.widx = btbif.ridx;
      btbif.istaken = 1'b1;
      btbif.bta = 32'h5C;
      btbif.pc = 32'h25;
      btbif.ridx = btbif.pc[3:2];
      #(PERIOD*2);
      btbif.baddr = btbif.pc;
      btbif.widx = btbif.ridx;
      btbif.istaken = 1'b1;
      btbif.bta = 32'h6C;
      btbif.pc = 32'h31;
      btbif.ridx = btbif.pc[3:2];
      #(PERIOD*2);
      btbif.baddr = btbif.pc;
      btbif.widx = btbif.ridx;
      btbif.istaken = 1'b1;
      btbif.bta = 32'h7C;

      //Test 2: check if pc hit and change of state of predictor 1
      btbif.pc = 32'h1F;
      btbif.ridx = btbif.pc[3:2];
      #(PERIOD*2);
      btbif.baddr = btbif.pc;
      btbif.widx = btbif.ridx;
      btbif.istaken = 1'b0;
      btbif.bta = 32'h5C;
   end
endprogram