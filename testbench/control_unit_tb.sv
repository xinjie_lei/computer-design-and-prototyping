// interface
`include "control_unit_if.vh"

// types
`include "cpu_types_pkg.vh"

// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 1 ns

module control_unit_tb;
   // clock period
   parameter PERIOD = 10;

   // signals
   logic CLK = 1, nRST;

   // clock
   always #(PERIOD/2) CLK++;

   // interface
   control_unit_if cuif();
   
   // test program
   test PROG (cuif);

   // dut
   control_unit ctrl (cuif);

endmodule

program test

  import cpu_types_pkg::*;
   
   (
    control_unit_if.cutb cuif
    );
   parameter PERIOD=10;
   initial begin
      //Test 1: JR
      cuif.InstruOp = 6'b000000;
      cuif.FuncOp = 6'b001000;
      cuif.Zero = 32'b0;
      #(PERIOD);

      //Test 2: BEQ
      cuif.InstruOp = 6'b000100;
      cuif.FuncOp = 6'b000000;
      cuif.Zero = 32'b0;
      #(PERIOD);
      
      //Test 3: BEQ
      cuif.InstruOp = 6'b000100;
      cuif.FuncOp = 6'b000000;
      cuif.Zero = 32'b1;
      #(PERIOD);
      
      //Test 4: JAL
      cuif.InstruOp = 6'b000011;
      cuif.FuncOp = 6'b000000;
      cuif.Zero = 32'b0;
      #(PERIOD);
      
   end
endprogram // test
   
