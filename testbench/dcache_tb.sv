// interface
`include "cache_control_if.vh"
`include "datapath_cache_if.vh"

// types
`include "cpu_types_pkg.vh"

// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 1 ns

module dcache_tb;
   // clock period
   parameter PERIOD = 10;

   // signals
   logic CLK = 1, nRST;

   // clock
   always #(PERIOD/2) CLK++;

   // interface
   cache_control_if ccif();
   datapath_cache_if dpif();
   
   // test program
   test PROG (CLK, nRST, dpif, ccif);

   // dut
   dcache DCACHE (CLK, nRST, dpif, ccif);
   
endmodule // icache_tb

program test
  import cpu_types_pkg::*;
   (
    input logic  CLK,
    output logic nRST,
    datapath_cache_if.dp dpif,
    cache_control_if.cc ccif
    );
   parameter PERIOD=10;
   initial begin
      //reset chip
      nRST = 1'b1;
      #(PERIOD);
      nRST = 1'b0;
      #(PERIOD);
      nRST = 1'b1;

      //test1: read miss
      #(PERIOD);
      ccif.dwait = 1'b1;

      dpif.dmemREN = 1'b1;
      dpif.dmemaddr = 32'h3C;
      
      #(PERIOD);
      ccif.dwait = 1'b0;
      ccif.dload = 32'h12341234;   
      #(PERIOD);
      ccif.dwait = 1'b0;
      ccif.dload = 32'h56785678;

      //test2: read hit
      #(PERIOD);
      ccif.dwait = 1'b1;

      dpif.dmemREN = 1'b1;
      dpif.dmemaddr = 32'h3C;
      #(PERIOD);

      //test3: read adjacent addr
      dpif.dmemREN = 1'b1;
      dpif.dmemaddr = 32'h38;
      #(PERIOD);
      dpif.dmemREN = 1'b0;
      
      //test3: write miss
      #(PERIOD);   
      dpif.dmemWEN = 1'b1;
      dpif.dmemaddr = 32'h40;
      dpif.dmemstore = 32'habcdabcd;
      
      #(PERIOD);
      ccif.dwait = 1'b0;
      ccif.dload = 32'h02020202;
      #(PERIOD);
      ccif.dwait = 1'b0;
      ccif.dload = 32'h03030303;

      //test4: write undirty
      #(PERIOD);
      ccif.dwait = 1'b1;
      dpif.dmemWEN = 1'b1;
      dpif.dmemaddr = 32'h40;
      dpif.dmemstore = 32'hedcbedcb;
      
      //test5: write lru
      #(PERIOD);
      dpif.dmemWEN = 1'b1;
      dpif.dmemaddr = 32'hC0;
      dpif.dmemstore = 32'hab98ab98;

      #(PERIOD);
      ccif.dwait = 1'b0;
      ccif.dload = 32'h04040404;     
      #(PERIOD);
      ccif.dwait = 1'b0;
      ccif.dload = 32'h05050505;

      //test6: write dirty
       #(PERIOD);
      ccif.dwait = 1'b1;
      dpif.dmemWEN = 1'b1;
      dpif.dmemaddr = 32'h80;
      dpif.dmemstore = 32'h66666666;

      #(PERIOD);
      ccif.dwait = 1'b0;
      #(PERIOD);
      ccif.dwait = 1'b0;
      #(PERIOD);
      ccif.dwait = 1'b0;
      ccif.dload = 32'h06060606;     
      #(PERIOD);
      ccif.dwait = 1'b0;
      ccif.dload = 32'h07070707;

      //test 7: halt
      #(PERIOD);
      dpif.halt = 1'b1;
      #(40 * PERIOD);
      
   end // initial begin
endprogram
