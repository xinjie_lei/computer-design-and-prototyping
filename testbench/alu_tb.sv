`include "alu_if.vh"
`include "cpu_types_pkg.vh"
// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 100 ps

module alu_tb;
   
   // interface
   alu_if aluif ();
   // test program
   test PROG (aluif.alutb);
   // DUT
`ifndef MAPPED
   alu DUT(aluif.alu);
`else
  alu DUT(
    .\alu.ina (aluif.ina),
    .\alu.inb (aluif.inb),
    .\alu.op (aluif.op),
    .\alu.result (aluif.result),
    .\alu.over (aluif.over),
    .\alu.zero (aluif.zero),
    .\alu.nega (aluif.nega)
  );
`endif
endmodule

program test
  import cpu_types_pkg::*;
   (
    alu_if.alutb tbif
    );
   parameter PERIOD = 10;
   initial begin
      $monitor("@%00g  ina = %h  inb = %h result = %h opcode = %b overflow = %b negation= %b zero = %b ",$time, tbif.ina, tbif.inb, tbif.result, tbif.op, tbif.over, tbif.nega, tbif.zero);
      
      //Test 0: logical shift left
      //$info("Test 0: Logical shift left");
      tbif.op = ALU_SLL;
      tbif.ina = 32'h3;
      tbif.inb = 32'h7;
      #(PERIOD);

      //Test 1: logical shift right
      //$info("Test 1: Logical shift right");
      tbif.op = ALU_SRL;
      tbif.ina = 32'h03;
      tbif.inb = 32'h07;
      #(PERIOD);
      
      //Test 2: ADD
      //$info("Test 2: Addition");
      tbif.op = ALU_ADD;
      tbif.ina = 32'h03;
      tbif.inb = 32'h07;
      #(PERIOD);
      
      //Test 3: SUB
      //$info("Test 3: SUBTRACTION");
      tbif.op = ALU_SUB;
      tbif.ina = 32'h0d;
      tbif.inb = 32'h10;
      #(PERIOD);

      //Test 4: AND
      //$info("Test 4: AND");
      tbif.op = ALU_AND;
      tbif.ina = 32'h03;
      tbif.inb = 32'h07;
      #(PERIOD);
      
      //Test 5: OR
      //$info("Test 5: OR");
      tbif.op = ALU_OR;
      tbif.ina = 32'h03;
      tbif.inb = 32'h07;
      #(PERIOD);
      
      //Test 6: XOR
      //$info("Test 6: XOR");
      tbif.op = ALU_XOR;
      tbif.ina = 32'h03;
      tbif.inb = 32'h07;
      #(PERIOD);
      
      //Test 7: NOR
      //$info("Test 7: NOR");
      tbif.op = ALU_NOR;
      tbif.ina = 32'h03;
      tbif.inb = 32'h07;
      #(PERIOD);
      
      //Test 8: SLT
      //$info("Test 8: Set less than");
      tbif.op = ALU_SLT;
      tbif.ina = 32'h03;
      tbif.inb = 32'hf000007;
      #(PERIOD);
      
      //Test 9: SLTU
      //$info("Test 9: Set less than unsigned");
      tbif.op = ALU_SLTU;
      tbif.ina = 32'h03;
      tbif.inb = 32'h07;
      #(PERIOD);
      
   end
endprogram
