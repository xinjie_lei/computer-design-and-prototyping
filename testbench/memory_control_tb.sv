// interface
`include "cache_control_if.vh"
`include "cpu_ram_if.vh"

// types
`include "cpu_types_pkg.vh"

// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 1 ns

module memory_control_tb;
   // clock period
   parameter PERIOD = 10;

   // signals
   logic CLK = 1, nRST;

   // clock
   always #(PERIOD/2) CLK++;

   // interface
   cache_control_if ccif();
   cpu_ram_if ramif();
   
   // test program
   test PROG (CLK,nRST,ccif);

   // dut
   memory_control  DUT_M (CLK,nRST,ccif);
   ram DUT_R (CLK,nRST,ramif);

   //connect ram and memory control
   assign ccif.ramstate = ramif.ramstate;
   assign ccif.ramload = ramif.ramload;
   assign ramif.ramREN = ccif.ramREN;
   assign ramif.ramWEN = ccif.ramWEN;
   assign ramif.ramaddr = ccif.ramaddr;
   assign ramif.ramstore = ccif.ramstore;

endmodule

program test

  import cpu_types_pkg::*;
   
   (
    input logic  CLK,
    output logic nRST,
    cache_control_if.cc ccif
    );
   parameter PERIOD=10;
   initial begin
      //reset chip
      nRST = 1'b1;
      #(PERIOD);
      nRST = 1'b0;
      #(PERIOD);
      nRST = 1'b1;
      ccif.ccwrite[0] = 1'b0;
      ccif.ccwrite[1] = 1'b0;
      
      //Test 1: choose icache0 to serve
      ccif.cctrans[0] = 1'b0;
      ccif.cctrans[1] = 1'b0;
      
      ccif.iREN[0] = 1'b1;
      ccif.iREN[1] = 1'b0;
      ccif.iaddr[0] = 32'h0;
      #(2 * PERIOD);
      //Test 2: choose icache1 to serve
      ccif.iREN[1] = 1'b1;
      ccif.iREN[0] = 1'b0;
      ccif.iaddr[1] = 32'h4;    
      #(2 * PERIOD);
      
      //TEST 3: choose icache0 to serve (oscilliation)
      ccif.iREN[0] = 1'b1;
      ccif.iREN[1] = 1'b1;
      ccif.iaddr[0] = 32'h8;
      #(4 * PERIOD);
      ccif.iREN[0] = 1'b0;
      ccif.iREN[1] = 1'b0;
      
      //Test 4: choose dcache0 to serve, mtc 
      ccif.cctrans[0] = 1'b1;
      ccif.cctrans[1] = 1'b0;
      ccif.dREN[0] = 1'b1;
      ccif.dREN[1] = 1'b0;
      ccif.dWEN[0] = 1'b0;
      ccif.dWEN[1] = 1'b0;
      #(PERIOD);
      ccif.cctrans[1] = 1'b1;
      ccif.dWEN[1] = 1'b0;
      #(PERIOD);
      ccif.daddr[0] = 32'h10;
      #(2 *PERIOD);
      ccif.daddr[0] = 32'h14;
      #(PERIOD);
      
      //Test 5: choose dcache1 to serve, ctc
      ccif.cctrans[0] = 1'b0;
      ccif.cctrans[1] = 1'b1;
      ccif.dREN[1] = 1'b1;
      ccif.dREN[0] = 1'b0;  
      ccif.dWEN[0] = 1'b0;
      ccif.dWEN[1] = 1'b0;  
      #(PERIOD);
      ccif.cctrans[0] = 1'b1;
      ccif.dWEN[0] = 1'b1;
      #(PERIOD);
      ccif.daddr[0] = 32'h0;
      ccif.dstore = 32'hBEEFBEEF;
      #(2 * PERIOD);
      ccif.daddr[0] = 32'h4;
      ccif.dstore = 32'hDEEDBEEF;
      #(PERIOD);
      
      //Test 6: Choose dcache0 to serve, mtc
      ccif.dREN[0] = 1'b1;
      ccif.dREN[1] = 1'b1;
      ccif.cctrans[0] = 1'b1;
      ccif.cctrans[1] = 1'b1;
      ccif.dWEN[0] = 1'b0;
      ccif.dWEN[1] = 1'b0;
      #(PERIOD);
      ccif.cctrans[1] = 1'b1;
      ccif.dWEN[1] = 1'b0;
      #(PERIOD);
      ccif.daddr[0] = 32'h0;
      #(2 * PERIOD);
      ccif.daddr[0] = 32'h4;
      #(2 * PERIOD);
      ccif.dREN[0] = 1'b0;
      #(PERIOD);

   end // initial begin
   
endprogram
