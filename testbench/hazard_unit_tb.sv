/*
 Xinjie Lei
 lei13@purdue.edu
 branch target buffer test bench
 */

// mapped needs this
`include "hazard_unit_if.vh"

// mapped timing needs this. 1ns is too fast
`timescale 1 ns / 1 ns

module hazard_unit_tb;

   // interface
   hazard_unit_if hzif ();
   // test program
   test PROG (hzif);
   // DUT
   hazard_unit DUT(hzif);


endmodule

program test(
	     hazard_unit_if.hztb hzif
	     );
   parameter PERIOD = 10;
   initial begin

      //Test 1: check halt
      hzif.Halt = 1'b1;
      hzif.Over = 1'b0;
      hzif.iscorrect = 1'b1;
      hzif.isjump = 1'b0;
      #(PERIOD);
      
      //Test 2: check branch
      hzif.Halt = 1'b0;
      hzif.Over = 1'b0;
      hzif.iscorrect = 1'b0;
      hzif.isjump = 1'b0;
      hzif.isbranch = 1'b1;
      hzif.isjump = 1'b0;
      #(PERIOD);

       //Test 3: check jump
      hzif.Halt = 1'b0;
      hzif.Over = 1'b0;
      hzif.iscorrect = 1'b0;
      hzif.isjump = 1'b0;
      hzif.isbranch = 1'b0;
      hzif.isjump = 1'b1;
      #(PERIOD);

      //Test 4: check load use
      hzif.idexdmemREN = 1'b1;
      hzif.idexRt = 32'h4;
      hzif.ifidRs = 32'h4;
      hzif.ifidRt = 32'h3;
      hzif.isjump = 1'b0;
      #(PERIOD);
      
   end
endprogram
