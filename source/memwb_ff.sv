/*
 Xinjie Lei
 lei13@purdue.edu
 
 MEM/WB register
 */

`include "ifid_if.vh"
`include "cpu_types_pkg.vh"

module memwb_ff
  import cpu_types_pkg::*;
   (
    input logic CLK,
    input logic nRST,
    memwb_if.memwb memwb
    );
   
   always_ff @ (posedge CLK, negedge nRST)begin
      if(!nRST) begin
	 memwb.npc <= 32'b0;
	 memwb.result <= 32'b0;
	 memwb.dmemload <= 32'b0;
	 memwb.Rd <= 5'b0;
	 memwb.RegSrc <= 2'b0;
	 memwb.RegWEN <= 1'b0;
	 memwb.Halt <= 1'b0;
	 memwb.memwbinstru <= 32'b0;
	 memwb.Over <= 1'b0;
      end
      else if (memwb.WEN)begin
	 memwb.npc <= memwb.npc_in;
	 memwb.result <= memwb.result_in;
	 memwb.dmemload <= memwb.dmemload_in;
	 memwb.Rd <= memwb.Rd_in;
	 memwb.RegSrc <= memwb.RegSrc_in;
	 memwb.Rd <= memwb.Rd_in;
	 memwb.RegWEN <= memwb.RegWEN_in;
	 memwb.Halt <= memwb.Halt_in;
	 memwb.memwbinstru <= memwb.memwbinstru_in;
	 memwb.Over <= memwb.Over_in;
      end
   end

endmodule // exmem register
