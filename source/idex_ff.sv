/*
 Xinjie Lei
 lei13@purdue.edu
 
 ID/EX register
 */

`include "ifid_if.vh"
`include "cpu_types_pkg.vh"

module idex_ff
  import cpu_types_pkg::*;
   (
    input logic CLK,
    input logic nRST,
    idex_if.idex idex
    );
   
   always_ff @ (posedge CLK, negedge nRST)begin
      if(!nRST) begin
	 idex.npc <= 32'b0;
	 idex.rdat1 <= 32'b0;
	 idex.rdat2 <= 32'b0;
	 idex.ExtImm <= 32'b0;
	 idex.Rs <= 5'b0;
	 idex.Rt <= 5'b0;
	 idex.Rd <= 5'b0;
	 idex.Shamt <= 5'b0;
	 idex.RegSrc <= 2'b0;
	 idex.RegDst <= 2'b0;
	 idex.InbSrc <= 2'b0;
	 idex.Beq <= 1'b0;
	 idex.Bne <= 1'b0;
	 idex.dmemREN <= 1'b0;
	 idex.dmemWEN <= 1'b0;
	 idex.RegWEN <= 1'b0;
	 idex.InaSrc <= 1'b0;
	 idex.ALUOp <= ALU_SLL;
	 idex.Halt <= 1'b0;
	 idex.idexinstru <= 32'b0;
	 idex.baddr = 32'b0;
	 idex.isbr <= 1'b0;
	 idex.ishit <= 1'b0;
	 idex.nedOver = 1'b0;
	 idex.Atomic <= 1'b0;
	 
      end
      else if (idex.WEN)begin
	 idex.npc <= idex.npc_in;
	 idex.rdat1 <= idex.rdat1_in;
	 idex.rdat2 <= idex.rdat2_in;
	 idex.ExtImm <= idex.ExtImm_in;
	 idex.Rs <= idex.Rs_in;
	 idex.Rt <= idex.Rt_in;
	 idex.Rd <= idex.Rd_in;
	 idex.Shamt <= idex.Shamt_in;	
	 idex.ALUOp <= idex.ALUOp_in;
	 idex.Atomic <= idex.Atomic_in;
	 
	 idex.idexinstru <= idex.idexinstru_in;		 
	 if(idex.Flush) begin
	    idex.RegSrc <= 2'b0;
	    idex.RegDst <= 2'b0;
	    idex.InbSrc <= 2'b0;
	    idex.Beq <= 1'b0;
	    idex.Bne <= 1'b0;
	    idex.dmemREN <= 1'b0;
	    idex.dmemWEN <= 1'b0;
	    idex.RegWEN <= 1'b0;
	    idex.InaSrc <= 1'b0;
	    idex.ALUOp <= ALU_SLL;
	    idex.Halt <= 1'b0;
	    idex.isbr <= 1'b0;
	    idex.ishit <= 1'b0;
	    idex.idexinstru <= 32'b0;
	    idex.nedOver <= 1'b0;
	    idex.Atomic <= 1'b0;
	    
	 end // if (idex.Flush)
	 else begin
	    idex.RegSrc <= idex.RegSrc_in;
	    idex.RegDst <= idex.RegDst_in;
	    idex.InbSrc <= idex.InbSrc_in;
	    idex.Beq <= idex.Beq_in;
	    idex.Bne <= idex.Bne_in;
	    idex.dmemREN <= idex.dmemREN_in;
	    idex.dmemWEN <= idex.dmemWEN_in;
	    idex.RegWEN <= idex.RegWEN_in;
	    idex.InaSrc <= idex.InaSrc_in;
	    idex.Halt <= idex.Halt_in;
	    idex.baddr <= idex.baddr_in;
	    idex.isbr <= idex.isbr_in;
	    idex.ishit <= idex.ishit_in;
	    idex.nedOver <= idex.nedOver_in;
	    idex.Atomic <= idex.Atomic_in;
	    
	 end
      end
   end

endmodule // IF/ID register