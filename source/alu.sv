/*
 Xinjie Lei
 lei13@purdue.edu
 
 alu - basic arithmetic operations
 */

`include "cpu_types_pkg.vh"
`include "alu_if.vh"

module alu
  import cpu_types_pkg::*;
   (
    alu_if.alu alu
    );

   
   assign alu.zero = (alu.result == 32'b0) ? 1'b1 : 1'b0;
   assign alu.nega = alu.result[31]; 
 
   always_comb begin
      alu.result=32'b0;
      alu.over=1'b0;
      case(alu.op) 
	ALU_SLL: begin
	   alu.result = alu.ina << alu.inb;
	end
	ALU_SRL: begin
	   alu.result = alu.ina >> alu.inb;
	end
	ALU_ADD: begin
	   alu.result = alu.ina + alu.inb;
	   alu.over = alu.ina[31] != alu.inb[31] ? 0 :
			  (alu.inb[31] == alu.result[31] ? 0 : 1);
	end
	ALU_SUB: begin
	   alu.result = alu.ina - alu.inb;
	   alu.over = alu.ina[31] == alu.inb [31] ? 0 :
			  (alu.ina[31] == alu.result[31] ? 0 : 1);
	end
	ALU_AND: begin
	   alu.result = alu.ina & alu.inb;
	end
	ALU_OR: begin
	   alu.result = alu.ina | alu.inb;
	end
	ALU_XOR: begin
	   alu.result = alu.ina ^ alu.inb;
	end
	ALU_NOR: begin
	   alu.result = ~ (alu.ina | alu.inb);
	end
	ALU_SLT: begin
	   alu.result = $signed(alu.ina) < $signed(alu.inb) ? 1'b1 : 1'b0;
	end
	ALU_SLTU: begin
	   alu.result = (alu.ina < alu.inb) ? 1'b1:1'b0;
	end
      endcase // case (alu.op)
   end // always_comb begin
   
endmodule// alu

	   
	
  

   
