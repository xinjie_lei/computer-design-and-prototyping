/*
 Xinjie Lei
 lei13@purdue.edu
 
 control unit generate control signal
 */
`include "control_unit_if.vh"
`include "cpu_types_pkg.vh"
module control_unit 
  import cpu_types_pkg::*;  
   (
    control_unit_if.cu cuif
    );
   
   always_comb begin
      cuif.Halt = 1'b0;
      cuif.RegDst = 2'b0;  
      cuif.RegSrc = 2'b0;
      cuif.InaSrc = 1'b0; 
      cuif.InbSrc = 2'b01;
      cuif.RegWEN = 1'b1;
      cuif.ExtOp = 1'b1;
      cuif.ALUOp = ALU_SLL;
      cuif.Jr = 1'b0;
      cuif.Jump = 1'b0;
      cuif.memREN = 1'b0;
      cuif.memWEN = 1'b0;
      cuif.Beq = 1'b0;
      cuif.Bne = 1'b0;
      cuif.nedOver = 1'b1;
      cuif.Atomic = 1'b0;
      
      case(cuif.InstruOp)
	RTYPE: begin
	   cuif.RegDst = 2'b01;
	   cuif.InbSrc = 2'b00;
	   case(cuif.FuncOp)
	     SLL: begin
		cuif.ALUOp = ALU_SLL;
		cuif.InbSrc = 2'b10;
	     end
	     SRL: begin
		cuif.ALUOp = ALU_SRL;
		cuif.InbSrc = 2'b10;
	     end
	     JR: begin
		cuif.RegWEN = 1'b0;
		cuif.Jump = 1'b1;
		cuif.Jr = 1'b1;
	     end
	     ADD: begin
		cuif.ALUOp = ALU_ADD;
	     end
	     ADDU: begin
		cuif.ALUOp = ALU_ADD;
		cuif.nedOver = 1'b0;
	     end
	     SUB: begin
		cuif.ALUOp = ALU_SUB;
	     end
	     SUBU:begin
		cuif.ALUOp = ALU_SUB;
		cuif.nedOver = 1'b0;
	     end
	     AND: begin
		cuif.ALUOp = ALU_AND;
	     end
	     OR: begin
		cuif.ALUOp = ALU_OR;
	     end
	     XOR: begin
		cuif.ALUOp = ALU_XOR;
	     end
	     NOR: begin
		cuif.ALUOp = ALU_NOR;
	     end
	     SLT: begin
		cuif.ALUOp = ALU_SLT;
	     end
	     SLTU: begin
		cuif.ALUOp = ALU_SLTU;
	     end
	   endcase // case (cuif.instru[5:0])
	end
	BEQ: begin
	   cuif.Beq = 1'b1;
	   cuif.InbSrc = 2'b00;
	   cuif.RegWEN = 1'b0;
	   cuif.ALUOp = ALU_SUB;
	end
	BNE: begin
	   cuif.Bne = 1'b1;
	   cuif.InbSrc = 2'b00;
	   cuif.RegWEN = 1'b0;
	   cuif.ALUOp = ALU_SUB;
	end
	ADDI: begin
	   cuif.ALUOp = ALU_ADD;
	end
	ADDIU: begin
	   cuif.ALUOp = ALU_ADD;
	   cuif.nedOver = 1'b0;
	end
	ANDI: begin
	   cuif.ExtOp = 1'b0;
	   cuif.ALUOp = ALU_AND;
	end
	LUI: begin
	   cuif.ExtOp = 1'b0;
	   cuif.InbSrc = 2'b11;
	   cuif.InaSrc = 1'b1;
	end
	LW: begin
	   cuif.memREN = 1'b1;
	   cuif.RegSrc = 2'b01;
	   cuif.ALUOp = ALU_ADD;
	end
	ORI: begin
	   cuif.ExtOp = 1'b0;
	   cuif.ALUOp = ALU_OR;
	end
	SLTI: begin
	   cuif.ALUOp = ALU_SLT;
	end
	SLTIU: begin
	   cuif.ALUOp = ALU_SLTU;
	end
	SW: begin
	   cuif.memWEN = 1'b1;
	   cuif.RegWEN = 1'b0;
	   cuif.RegSrc = 2'b01;
	   cuif.ALUOp = ALU_ADD;
	end
	XORI: begin
	   cuif.ExtOp = 1'b0;
	   cuif.ALUOp = ALU_XOR;
	end
	J: begin
	   cuif.RegWEN = 1'b0;
	   cuif.Jump = 1'b1;
	end
	JAL: begin
	   cuif.Jump = 1'b1;
	   cuif.RegDst = 2'b10;
	   cuif.RegSrc = 2'b10;
	end
	LL: begin
	   cuif.memREN = 1'b1;
	   cuif.RegSrc = 2'b01;
	   cuif.ALUOp = ALU_ADD;
	   cuif.Atomic = 1'b1;
	end
	SC: begin
	   cuif.memWEN = 1'b1;
	   cuif.RegSrc = 2'b01;
	   cuif.ALUOp = ALU_ADD;
	   cuif.Atomic = 1'b1;
	end
	HALT: begin
	   cuif.Halt = 1'b1;
    	   cuif.RegWEN = 1'b0;
	end
      endcase 
   end // always_comb begin
   

endmodule
