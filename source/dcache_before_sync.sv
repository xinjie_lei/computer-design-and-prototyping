/*
 Xinjie Lei
 lei13@purdue.edu
 
 this block holds the dcache
 */
`include "datapath_cache_if.vh"
`include "cache_control_if.vh"
`include "cpu_types_pkg.vh"

module dcache
  import cpu_types_pkg::*;
   (
    input logic CLK, nRST,
    datapath_cache_if.dcache dpif,
    cache_control_if ccif
    );
   parameter CPUID = 0;
   
   typedef struct packed {
			  logic [DTAG_W-1:0] tag;
			  word_t [1:0] data;
			  logic valid;
			  logic dirty;
			  }block_t;

typedef struct packed {
		       logic lru;
		       block_t [1:0] block;
		       }set_t;

typedef enum logic [3:0] {idle, waitmtc, fetch1, fetch2, ccwb1, ccwb2, wb1, wb2, flush1,flush2, flush3, flush4, halt} stateType;
stateType state, nstate;

//basic cache
set_t set[8];
dcachef_t fr;
logic hit, hit_w, hit_r, match0, match1, hbsel;
word_t faddr;
logic [2:0] flushidx;
//coherence
logic hit_s, match0_s, match1_s, hbsel_s;
dcachef_t fr_s;
//synchronization
word_t linkreg;


//basic cache check
assign fr.tag = dpif.dmemaddr[31:6];
assign fr.idx = dpif.dmemaddr[5:3];
assign fr.blkoff = dpif.dmemaddr[2];
assign match0 = (fr.tag == set[fr.idx].block[0].tag) && set[fr.idx].block[0].valid;
assign match1 = (fr.tag == set[fr.idx].block[1].tag) && set[fr.idx].block[1].valid;
assign hit_w = match0 || match1;
assign hit_r = (match0 || match1) && (!ccif.ccinv[CPUID] || (ccif.ccinv[CPUID] && (fr != fr_s)));
assign hbsel = match1;
			     
//snoop cache check
assign fr_s.tag = ccif.ccsnoopaddr[CPUID][31:6];
assign fr_s.idx = ccif.ccsnoopaddr[CPUID][5:3];
assign fr_s.blkoff = ccif.ccsnoopaddr[CPUID][2];
assign match0_s = (fr_s.tag == set[fr_s.idx].block[0].tag) && set[fr_s.idx].block[0].valid;
assign match1_s = (fr_s.tag == set[fr_s.idx].block[1].tag) && set[fr_s.idx].block[1].valid;
assign hit_s = match0_s || match1_s;
assign hbsel_s = match1_s;


//flushidx counter
always_ff @(posedge CLK, negedge nRST) begin
   if (!nRST) begin
      flushidx <= 0;
   end
   else if (state == flush4 && nstate != flush4) begin
      flushidx <= flushidx + 32'h1;
   end
end
   //update cache and state machine
   always_ff @(posedge CLK, negedge nRST) begin
      if(!nRST) begin
	 state <= idle;
      end
      else begin
	 state <= nstate;
      end
   end // always_ff @ (posedge CLK, negedge nRST)
   //next state logic
   always_comb begin
      nstate = state;
      case(state)
	idle: begin
	   if (dpif.halt && !ccif.ccwait[CPUID]) begin
	      nstate = flush1;
	   end
	   else if(ccif.ccwait[CPUID] && (!hit_s || (hit_s && !set[fr_s.idx].block[hbsel_s].dirty))) begin
	      nstate = waitmtc;
	   end
	   else if(ccif.ccwait[CPUID] && hit_s && set[fr_s.idx].block[hbsel_s].dirty) begin
	      nstate = ccwb1;
	   end
	   else if(((dpif.dmemWEN && !hit_w) || (dpif.dmemREN && !hit_r)) && set[fr.idx].block[set[fr.idx].lru].dirty && set[fr.idx].block[set[fr.idx].lru].valid) begin
	      nstate = wb1;
	   end
	   else if((dpif.dmemREN && !hit_r) || (dpif.dmemWEN && !hit_w)) begin
	      nstate = fetch1;
	   end
	end // case: idle
	waitmtc: begin
	   if(!ccif.ccwait[CPUID]) begin
	      nstate = idle;
	   end
	end
	fetch1: begin
	   if(!ccif.dwait[CPUID]) begin
	      nstate = fetch2;
	   end
	end
	fetch2: begin
	   if(!ccif.dwait[CPUID]) begin
	      nstate = idle;
	   end
	end
	wb1: begin
	   if(!ccif.dwait[CPUID]) begin
	      nstate = wb2;
	   end
	end
	wb2: begin
	   if(!ccif.dwait[CPUID]) begin
	      nstate = fetch1;
	   end
	end
	ccwb1: begin
	   if(!ccif.dwait[CPUID]) begin
	      nstate = ccwb2;
	   end
	end
	ccwb2: begin
	   if(!ccif.dwait[CPUID]) begin
	      nstate = idle;
	   end
	end
	flush1: begin //set[1]data[1]
	   if(ccif.ccwait[CPUID] && hit_s && set[fr_s.idx].block[hbsel_s].dirty) begin
	      nstate = ccwb1;
	   end
	   else if(ccif.ccwait[CPUID] && (!hit_s || (hit_s && !set[fr_s.idx].block[hbsel_s].dirty))) begin
	      nstate = waitmtc;
	   end
	   else if(!ccif.dWEN[CPUID] || !ccif.dwait[CPUID]) begin
	      nstate = flush2;
	   end
	end
	flush2: begin//set[1]data[0]
	   if(ccif.ccwait[CPUID] && hit_s && set[fr_s.idx].block[hbsel_s].dirty) begin
	      nstate = ccwb1;
	   end
	   else if(ccif.ccwait[CPUID] && (!hit_s || (hit_s && !set[fr_s.idx].block[hbsel_s].dirty))) begin
	      nstate = waitmtc;
	   end
	   else if(!ccif.dWEN[CPUID] || !ccif.dwait[CPUID]) begin
	      nstate = flush3;
	   end
	end
	flush3: begin//set[0]data[1]
	   if(ccif.ccwait[CPUID] && hit_s && set[fr_s.idx].block[hbsel_s].dirty) begin
	      nstate = ccwb1;
	   end
	   else if(ccif.ccwait[CPUID] & (!hit_s || (hit_s && !set[fr_s.idx].block[hbsel_s].dirty))) begin
	      nstate = waitmtc;
	   end
	   else if(!ccif.dWEN[CPUID] || !ccif.dwait[CPUID]) begin
	      nstate = flush4;
	   end
	end
	flush4: begin//set[0]data[0]
	   if(ccif.ccwait[CPUID] && hit_s && set[fr_s.idx].block[hbsel_s].dirty) begin
	      nstate = ccwb1;
	   end
	   else if(ccif.ccwait[CPUID] && (!hit_s || (hit_s && !set[fr_s.idx].block[hbsel_s].dirty))) begin
	      nstate = waitmtc;
	   end
	   else if(!ccif.dWEN[CPUID] || !ccif.dwait[CPUID]) begin
	      if(flushidx == 32'h7) begin
		 nstate = halt;
	      end
	      else begin
		 nstate = flush1;
	      end
	   end
	end
	halt: begin
	   nstate = halt;
	end
      endcase // case (state)
   end // always_comb begin
   
   //output logic
   always_comb begin
      dpif.dhit = 1'b0;
      dpif.dmemload = 32'hBAD1BAD1;
      dpif.flushed = 1'b0;
      ccif.dREN[CPUID] = 1'b0;
      ccif.dWEN[CPUID] = 1'b0;
      ccif.daddr[CPUID] = 32'hBAD1BAD1;
      ccif.dstore[CPUID] = 32'hBAD1BAD1;
      ccif.ccwrite[CPUID] = 1'b0;
      ccif.cctrans[CPUID] = 1'b0;
      case(state)
	idle: begin     
	   if(dpif.dmemREN & hit_r) begin
	      dpif.dmemload = set[fr.idx].block[hbsel].data[fr.blkoff];
	      dpif.dhit = 1'b1;
	   end
	   if(dpif.dmemWEN & hit_w) begin
	      ccif.ccwrite[CPUID] = 1'b1;
	      ccif.daddr[CPUID] = {fr.tag, fr.idx, 3'b000};
	      dpif.dhit = 1'b1;
	   end
	end // case: idle
	waitmtc: begin
	   ccif.cctrans[CPUID] = 1'b1;
	   ccif.dWEN[CPUID] = 1'b0;
	end
	fetch1: begin
	   ccif.cctrans[CPUID] = 1'b1;
	   ccif.dREN[CPUID] = 1'b1;
	   ccif.daddr[CPUID] = {fr.tag, fr.idx, 3'b000};
	end
	fetch2: begin
	   ccif.cctrans[CPUID] = 1'b1;
	   ccif.dREN[CPUID] = 1'b1;
	   ccif.daddr[CPUID] = {fr.tag, fr.idx, 3'b100};
	end
	wb1: begin
	   ccif.dWEN[CPUID] = 1'b1;
	   ccif.daddr[CPUID] = {set[fr.idx].block[set[fr.idx].lru].tag, fr.idx, 3'b000};
	   ccif.dstore[CPUID] = set[fr.idx].block[set[fr.idx].lru].data[0];
	end
	wb2: begin
	   ccif.dWEN[CPUID] = 1'b1;
	   ccif.daddr[CPUID] = {set[fr.idx].block[set[fr.idx].lru].tag, fr.idx, 3'b100};
	   ccif.dstore[CPUID] = set[fr.idx].block[set[fr.idx].lru].data[1];
	end // case: wb2
	ccwb1: begin
	   ccif.cctrans[CPUID] = 1'b1;
	   ccif.dWEN[CPUID] = 1'b1;
	   ccif.daddr[CPUID] = {set[fr_s.idx].block[hbsel_s].tag, fr_s.idx, 3'b000};
	   ccif.dstore[CPUID] = set[fr_s.idx].block[hbsel_s].data[0];
	end
	ccwb2: begin
	   ccif.cctrans[CPUID] = 1'b1;
	   ccif.dWEN[CPUID] = 1'b1;
	   ccif.daddr[CPUID] = {set[fr_s.idx].block[hbsel_s].tag, fr_s.idx, 3'b100};
	   ccif.dstore[CPUID] = set[fr_s.idx].block[hbsel_s].data[1];
	end
	flush1: begin
	   ccif.dWEN[CPUID] = set[flushidx].block[1].dirty;
	   ccif.daddr[CPUID] = {set[flushidx].block[1].tag, flushidx, 3'b100};
	   ccif.dstore[CPUID] = set[flushidx].block[1].data[1];
	end
	flush2: begin
	   ccif.dWEN[CPUID] = set[flushidx].block[1].dirty;
	   ccif.daddr[CPUID] = {set[flushidx].block[1].tag, flushidx, 3'b000};
	   ccif.dstore[CPUID] = set[flushidx].block[1].data[0];
	end
	flush3: begin
	   ccif.dWEN[CPUID] = set[flushidx].block[0].dirty;
	   ccif.daddr[CPUID] = {set[flushidx].block[0].tag, flushidx, 3'b100};
	   ccif.dstore[CPUID] = set[flushidx].block[0].data[1];
	end
	flush4: begin
	   ccif.dWEN[CPUID] = set[flushidx].block[0].dirty;
	   ccif.daddr[CPUID] = {set[flushidx].block[0].tag, flushidx, 3'b000};
	   ccif.dstore[CPUID] = set[flushidx].block[0].data[0];
	end
	halt: begin
	   ccif.dWEN[CPUID] = 1'b0;
	   ccif.cctrans[CPUID] = 1'b1;
	   dpif.flushed = 1'b1;
	end
      endcase // case (state)
   end // always_comb begin
   
   //update cache
   always_ff @(posedge CLK, negedge nRST) begin
      if(!nRST) begin
	 set <= '{default:0};
      end
      else begin
	 case(state)
	   idle: begin
	      if(dpif.dmemWEN & hit_w) begin
		 set[fr.idx].block[hbsel].data[fr.blkoff] <= dpif.dmemstore;
		 set[fr.idx].block[hbsel].dirty <= 1'b1;
		 set[fr.idx].lru = !hbsel;
	      end
	      if(dpif.dmemREN & hit_r) begin
		 set[fr.idx].lru = !hbsel;
	      end
	      if(ccif.ccinv[CPUID] & hit_s) begin
		 set[fr_s.idx].block[hbsel_s].valid <= 1'b0;
		 set[fr_s.idx].block[hbsel_s].dirty <= 1'b0;
	      end					      
	   end
	   fetch1: begin
	      set[fr.idx].block[set[fr.idx].lru].data[0] <= ccif.dload[CPUID];
	   end
	   fetch2: begin
	      set[fr.idx].block[set[fr.idx].lru].tag <= fr.tag;
	      set[fr.idx].block[set[fr.idx].lru].data[1] <= ccif.dload[CPUID];
	      set[fr.idx].block[set[fr.idx].lru].valid <= 1'b1;
	   end
	   wb2: begin
	      set[fr.idx].block[set[fr.idx].lru].dirty <= 1'b0;
	   end
	   ccwb2: begin
	      set[fr_s.idx].block[hbsel_s].dirty <= 1'b0;
	   end
	   flush2: begin
	      set[flushidx].block[1].valid = 1'b0;
	   end
	   flush4: begin
	      set[flushidx].block[0].valid = 1'b0;
	   end
	 endcase // case (state)
      end
   end // always_ff @ (posedge CLK, negedge nRST)
endmodule // caches
