/*
 Xinjie Lei
 lei13@purdue.edu
 
 hazard unit interface
 */

//all types
`include "hazard_unit_if.vh"
`include "cpu_types_pkg.vh"

module hazard_unit
  import cpu_types_pkg::*;
   (
    hazard_unit_if.hz hzif
    );
   //logic 	halt, load_use, valid_id;
   
   always_comb begin
      hzif.pcWEN = 1'b1;
      hzif.ifidWEN = hzif.ihit;
      hzif.idexWEN = hzif.ihit;
      hzif.exmemWEN = hzif.ihit;
      hzif.memwbWEN = hzif.ihit;
      hzif.ifidFlush = 1'b0;
      hzif.idexFlush = 1'b0;
      hzif.exmemFlush = 1'b0;
      
      if(hzif.exmemdmemWEN || hzif.exmemdmemREN) begin
	 hzif.pcWEN = hzif.dhit;
	 hzif.ifidWEN = hzif.dhit;	 
	 hzif.idexWEN = hzif.dhit;
	 hzif.exmemWEN = hzif.dhit;
	 hzif.memwbWEN = hzif.dhit;
      end    
      //halt -- IF-dont write ID-remain in halt EX,MEM,WB finish previous instruction
      if((hzif.Halt || hzif.Over) && hzif.iscorrect && !hzif.isjump)begin
	 hzif.pcWEN = 1'b0;
	 hzif.ifidWEN = 1'b0;
      end // if (hzif.Halt)
      if(hzif.isbranch && !hzif.iscorrect) begin
	 hzif.ifidFlush = 1'b1;
	 hzif.idexFlush = 1'b1;
      end
      if(hzif.isjump && !(hzif.isbranch && !hzif.iscorrect)) begin
	 hzif.ifidFlush = 1'b1;
      end
      //load-use
      if((hzif.idexdmemREN || hzif.idexdmemWEN && hzif.idexAtomic) && (hzif.idexRt != 0) && ((hzif.idexRt == hzif.ifidRs) || (hzif.idexRt == hzif.ifidRt)) && !(hzif.isbranch && !hzif.iscorrect) && !hzif.isjump) begin
	 hzif.pcWEN = 1'b0;
	 hzif.ifidWEN = 1'b0;
	 hzif.idexFlush = 1'b1;
      end   
      	
   end // always_comb begin
   
endmodule
