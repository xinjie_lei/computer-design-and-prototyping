/*
 Xinjie Lei
 lei13@purdue.edu
 
 datapath contains register file, control, hazard,
 muxes, and glue logic for processor
 */

// data path interface
`include "datapath_cache_if.vh"
`include "alu_if.vh"
`include "register_file_if.vh"
`include "control_unit_if.vh"
`include "hazard_unit_if.vh"
`include "ifid_if.vh"
`include "idex_if.vh"
`include "exmem_if.vh"
`include "memwb_if.vh"
`include "branch_target_buffer_if.vh"

// alu op, mips op, and instruction type
`include "cpu_types_pkg.vh"

module datapath (
		 input logic CLK, nRST,
		 datapath_cache_if.dp dpif
		 );
   // import types
   import cpu_types_pkg::*;
   //Interface
   alu_if aluif();
   register_file_if rfif();
   control_unit_if cuif();
   hazard_unit_if hzif();
   branch_target_buffer_if btbif();
   ifid_if ifid();
   idex_if idex();
   exmem_if exmem();
   memwb_if memwb();
   
   //Instantiate modules 
   alu ALU(aluif);
   register_file RF(CLK, nRST, rfif);
   control_unit CU(cuif);
   hazard_unit HZ(hzif);
   branch_target_buffer BTB(CLK, nRST, btbif);
   ifid_ff IFID_FF(CLK, nRST, ifid);
   idex_ff IDEX_FF(CLK, nRST, idex);
   exmem_ff EXMEM_FF(CLK, nRST, exmem);
   memwb_ff MEMWB_FF(CLK, nRST, memwb);
   
   // pc init
   parameter PC_INIT = 0;//initialize pc addr
   //variables
   logic PCWEN, n_over, over, istaken, isbranch;
   word_t PC4, imm_extend, rf_wdata, ina, inb, bta, jta;
   
   //assign logic
   //IF stage
   assign PCWEN = dpif.ihit && hzif.pcWEN;
   assign PC4 = dpif.imemaddr + 32'h4;
   assign ifid.npc_in = PC4;
   
   assign ifid.baddr_in = dpif.imemaddr;
   assign ifid.isbr_in = btbif.br && dpif.ihit;
   assign ifid.ishit_in = btbif.hit && dpif.ihit;
   assign ifid.instruc_in = dpif.imemload;
  
   assign btbif.pc = dpif.imemaddr;
   assign btbif.ridx = dpif.imemaddr[3:2];
   assign btbif.widx = idex.baddr[3:2];  
   assign btbif.bta = bta;
   assign btbif.ishit = idex.ishit;
   assign btbif.istaken = istaken;
   assign btbif.isbranch = isbranch;
   assign btbif.baddr = idex.baddr;
   
   assign dpif.imemREN = (!memwb.Halt && !memwb.Over) ? 1'b1 : 1'b0;
   

   //ID stage
   assign isbranch = idex.Bne || idex.Beq;
   
   assign idex.npc_in = ifid.npc;
   /************************************/
   assign idex.idexinstru_in = ifid.instruc;
   /**********************************/
   assign rfif.rsel1 = ifid.instruc[25:21];//rs
   assign rfif.rsel2 = ifid.instruc[20:16];//rt
   assign rfif.wsel = memwb.Rd;
   assign rfif.WEN = memwb.RegWEN;
   assign rfif.wdat = rf_wdata;

   assign idex.baddr_in = ifid.baddr;
   assign idex.isbr_in = ifid.isbr;
   assign idex.ishit_in = ifid.ishit;
   assign idex.Rs_in = ifid.instruc[25:21];
   assign idex.Rt_in = ifid.instruc[20:16];
   assign idex.Rd_in = ifid.instruc[15:11];
   assign idex.ExtImm_in = imm_extend;
   assign idex.Shamt_in = ifid.instruc[10:6];
   assign idex.rdat1_in = rfif.rdat1;
   assign idex.rdat2_in = rfif.rdat2; 
   assign idex.RegDst_in = cuif.RegDst;
   assign idex.ALUOp_in = cuif.ALUOp;
   assign idex.InaSrc_in = cuif.InaSrc;
   assign idex.InbSrc_in = cuif.InbSrc;
   assign idex.RegSrc_in = cuif.RegSrc;
   assign idex.RegWEN_in = cuif.RegWEN;
   assign idex.dmemREN_in = cuif.memREN;
   assign idex.dmemWEN_in = cuif.memWEN;
   assign idex.Atomic_in = cuif.Atomic;
   assign idex.Beq_in = cuif.Beq;
   assign idex.Bne_in = cuif.Bne;
   assign idex.Halt_in = cuif.Halt;
   assign idex.nedOver_in = cuif.nedOver;
   
   assign cuif.InstruOp = ifid.instruc[31:26];
   assign cuif.FuncOp = ifid.instruc[5:0];
   
   //EXE Stage 
   assign exmem.npc_in = idex.npc;
   assign exmem.dmemREN_in = idex.dmemREN;
   assign exmem.dmemWEN_in = idex.dmemWEN;
   assign exmem.RegWEN_in = idex.RegWEN;
   assign exmem.rdat1_in = idex.rdat1;
   assign exmem.result_in = aluif.result;
   assign exmem.dmemstore_in = inb;
   assign exmem.RegSrc_in = idex.RegSrc;
   assign exmem.Over_in = over || n_over;
   assign exmem.Atomic_in = idex.Atomic;
   assign exmem.Halt_in = idex.Halt;
   
   assign aluif.op = idex.ALUOp;
    /************************************/
   assign exmem.exmeminstru_in = idex.idexinstru;
   /**********************************/
   //MEM Stage
   assign memwb.npc_in = exmem.npc;
   assign memwb.result_in = exmem.result;
   assign memwb.Rd_in = exmem.Rd;
   assign memwb.RegSrc_in = exmem.RegSrc;
   assign memwb.RegWEN_in = exmem.RegWEN;
   assign memwb.Halt_in = exmem.Halt;
   assign memwb.Over_in = exmem.Over;
   assign memwb.dmemload_in = dpif.dmemload;
    /************************************/
   assign memwb.memwbinstru_in = exmem.exmeminstru;
   /**********************************/
   //Hazard Unit
   assign hzif.idexRt = idex.Rt;
   assign hzif.ifidRt = ifid.instruc[20:16];
   assign hzif.ifidRs = ifid.instruc[25:21];
   assign hzif.ihit = dpif.ihit;
   assign hzif.dhit = dpif.dhit;
   assign hzif.idexdmemREN = idex.dmemREN;
   assign hzif.idexdmemWEN = idex.dmemWEN;
   assign hzif.idexAtomic = idex.Atomic;
   assign hzif.exmemdmemREN = exmem.dmemREN;
   assign hzif.exmemdmemWEN = exmem.dmemWEN;
   assign hzif.Halt = cuif.Halt;
   assign hzif.iscorrect = !(idex.isbr ^ istaken);
   assign hzif.isjump = cuif.Jump;
   assign hzif.isbranch = isbranch;  
   assign hzif.Over = over || n_over;
   
   assign ifid.WEN = hzif.ifidWEN;
   assign idex.WEN = hzif.idexWEN;
   assign exmem.WEN = hzif.exmemWEN;
   assign memwb.WEN = hzif.memwbWEN;
   assign ifid.Flush = hzif.ifidFlush;
   assign idex.Flush = hzif.idexFlush;
   assign exmem.Flush = hzif.exmemFlush;
   /******************************************/
   assign dpif.datomic = exmem.Atomic;
   assign dpif.dmemaddr = exmem.result;
   assign dpif.dmemREN = exmem.dmemREN;
   assign dpif.dmemWEN = exmem.dmemWEN;
   assign dpif.dmemstore = exmem.dmemstore;
   
   //halt
   assign dpif.halt = (memwb.Halt || memwb.Over) ? 1'b1:1'b0;
   
   /**********************************IF Stage*********************************/
   //Program Counter -Reg
   always_ff @(posedge CLK, negedge nRST) begin
      if(!nRST) begin
	 dpif.imemaddr <= PC_INIT;
      end
      else begin
	 if (!idex.isbr && istaken) begin
	    dpif.imemaddr <= bta;
	 end
	 else if (idex.isbr && !istaken) begin
	    dpif.imemaddr <= btbif.baddr + 32'h4;
	 end
	 else if (cuif.Jump && !istaken) begin
	    dpif.imemaddr <= jta;
	 end	 
	 else if (PCWEN) begin  
	    if(btbif.br) begin
	       dpif.imemaddr <= btbif.taddr;
	    end
	    else begin
	       dpif.imemaddr <= PC4;
	    end
	 end
      end // else: !if(!nRST)
   end // always_ff @ (posedge CLK, negedge nRST)
   
   /*********************************ID Stage**********************************/
   
   //extender
   always_comb begin
      imm_extend = {16'b0, ifid.instruc[15:0]};
      if(cuif.ExtOp) begin
	 imm_extend = {{16{ifid.instruc[15]}}, ifid.instruc[15:0]};
      end
   end
   always_comb begin
      jta = {ifid.npc[31:28],(imm_extend << 2)};
      if(cuif.Jr == 1'b1) begin
	 jta = rfif.rdat1;
	 if(idex.RegWEN && (idex.Rd != 0) && (idex.Rd == ifid.instruc[25:21])) begin
	    jta = idex.rdat1;
	 end
	 else if(exmem.RegWEN && (exmem.Rd != 0) && (exmem.Rd == ifid.instruc[25:21]))begin
	    if(exmem.dmemREN) begin
	       jta = dpif.dmemload;
	    end
	    else begin
	       jta = exmem.rdat1;
	    end
	 end
      end
   end
   /**********************************EXE Stage********************************/
   //ALU Source select
   //Ina
   always_comb begin
      aluif.ina = ina;     
      if(idex.InaSrc) begin
	 aluif.ina = idex.ExtImm;
      end
   end
   
   always_comb begin
      aluif.inb = 32'h0;
      case(idex.InbSrc)
	2'b00: begin
	   aluif.inb = inb;
	end
	2'b01: begin
	   aluif.inb = idex.ExtImm;
	end
	2'b10: begin
	   aluif.inb = idex.Shamt;
	end
	2'b11: begin
	   aluif.inb = 32'h10;//LUI
	end
      endcase // case (cuif.ALUSrc)
   end // always_comb begin

   always_comb begin
      ina = idex.rdat1;
      if(exmem.RegWEN && (exmem.Rd != 0) && (exmem.Rd == idex.Rs))begin
	 ina = exmem.result;
      end
      else if(memwb.RegWEN && (memwb.Rd != 0) && (memwb.Rd == idex.Rs)) begin
	 ina = rf_wdata;
      end
   end

   always_comb begin
      inb = idex.rdat2;
      if(exmem.RegWEN && (exmem.Rd != 0) && (exmem.Rd == idex.Rt) && dpif.datomic)begin
	 inb = dpif.dmemload;
      end
      else if(exmem.RegWEN && (exmem.Rd != 0) && (exmem.Rd == idex.Rt))begin
	 inb = exmem.result;
      end
      else if(memwb.RegWEN && (memwb.Rd != 0) && (memwb.Rd == idex.Rt)) begin
	 inb = rf_wdata;
      end
   end
   
   always_comb begin
      exmem.Rd_in = idex.Rt;
      if(idex.RegDst == 2'b01) begin
	 exmem.Rd_in = idex.Rd;
      end
      else if (idex.RegDst == 2'b10) begin
	 exmem.Rd_in = 32'h1F;
      end
   end

   always_ff @(posedge CLK, negedge nRST) begin
      if(!nRST) begin
	 over <= 1'b0;
      end
      else if (!exmem.Over) begin
	 over <= n_over;
      end
   end
   
   assign n_over = aluif.over && idex.nedOver;
   
   //compute branch target address  
   assign bta = (idex.ExtImm << 2) + idex.npc;
   
   assign istaken = idex.Beq & aluif.zero || idex.Bne & !aluif.zero;
   /*******************************MEM Stage***********************************/
   /*always_comb begin
      if(exmem.RegWEN && memwb.RegWEN && (exmem.Rd == memwb.Rd)) begin
	 dpif.dmemstore = rf_wdata;
      end
      else begin
	 dpif.dmemstore = exmem.dmemstore;
      end
   end*/
   /*******************************WB Stage***********************************/
   always_comb begin
      rf_wdata = memwb.result;
      if(memwb.RegSrc == 2'b01) begin
	 rf_wdata = memwb.dmemload;
      end
      if(memwb.RegSrc == 2'b10) begin
	 rf_wdata = memwb.npc;
      end
   end
   
endmodule
