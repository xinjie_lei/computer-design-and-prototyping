/*
  Xinjie Lei
  lei13@purdue.edu
 
  memory control unit
*/

// interface include
`include "cache_control_if.vh"

// memory types
`include "cpu_types_pkg.vh"

module memory_control (
		       input CLK, nRST,
		       cache_control_if.cc ccif
		       );
   // type import
   import cpu_types_pkg::*;

   typedef enum 	     logic [3:0] {idle, inv, snoop, mtc1, mtc2, ctc1, ctc2} stateType;
   stateType state, nstate;
   logic 		     iserv, n_iserv, rdc, sdc, nsdc, nrdc; //rdc - requestor sdc - snooped cache/responser
   word_t invaddr0, ninvaddr0, invaddr1, ninvaddr1;
   
   assign ccif.iload[0] = ccif.ramload;
   assign ccif.iload[1] = ccif.ramload;
   
   always_ff @ (posedge CLK, negedge nRST) begin
      if(!nRST) begin
	 state <= idle;	
      end
      else begin
	 state <= nstate;	 	
      end
   end
   always_ff @ (posedge CLK, negedge nRST) begin
      if(!nRST) begin
	 iserv <= 1'b0;
	 invaddr0 <= 32'b0;
	 invaddr1 <= 32'b0;
	 rdc <= 1'b0;
	 sdc <= 1'b0;
      end
      else begin
	 iserv <= n_iserv;
	 invaddr0 <= ninvaddr0;
	 invaddr1 <= ninvaddr1;
	 rdc <= nrdc;
	 sdc <= nsdc;
      end // else: !if(!nRST)
   end
   //decide icache to serve
   always_comb begin
      n_iserv = iserv;
      if ((state == idle) && (ccif.ramstate == ACCESS || ccif.ramstate == FREE)) begin
	 if (ccif.iREN[0] && !ccif.iREN[1]) begin
	    n_iserv = 1'b0;
	 end
	 if (ccif.iREN[1] && !ccif.iREN[0]) begin
	    n_iserv = 1'b1;
	 end
	 if (ccif.iREN[0] && ccif.iREN[1]) begin
	    n_iserv = !iserv;
	 end
      end
   end // always_comb begin
   
    //decide dcache to serve, dcache0 has priority
   always_comb begin
      nrdc = rdc;
      nsdc = sdc;
      ninvaddr0 = invaddr0;
      ninvaddr1 = invaddr1;
      if(state == idle) begin
	 if (ccif.ccwrite[0] && !ccif.ccwrite[1]) begin
	    nrdc = 1'b0;
	    nsdc = 1'b1;
	    ninvaddr1 = ccif.daddr[0];
	 end
	 else if (ccif.ccwrite[1] && !ccif.ccwrite[0]) begin
	    nrdc = 1'b1;
	    nsdc = 1'b0;
	    ninvaddr0 = ccif.daddr[1];
	 end
	 else if (ccif.ccwrite[0] && ccif.ccwrite[1]) begin
	    nrdc = 1'b1;
	    nsdc = 1'b1;
	    ninvaddr1 = ccif.daddr[0];
	    ninvaddr0 = ccif.daddr[1];
	 end
	 else if (ccif.cctrans[0] && ccif.dREN[0] && !(ccif.dWEN[0] || ccif.dWEN[1])) begin
	    nrdc = 1'b0;
	    nsdc = 1'b1;
	 end
	 else if (ccif.cctrans[1] && ccif.dREN[1] && !(ccif.dWEN[0] || ccif.dWEN[1])) begin
	    nrdc = 1'b1;
	    nsdc = 1'b0;
	 end
      end // if (state == idle)
      else if (state == inv) begin
	   if (ccif.ccwrite[0] && !ccif.ccwrite[1]) begin
	      nrdc = 1'b0;
	      nsdc = 1'b1;
	      ninvaddr1 = ccif.daddr[0];
	   end
	   else if (ccif.ccwrite[1] && !ccif.ccwrite[0]) begin
	      nrdc = 1'b1;
	      nsdc = 1'b0;
	      ninvaddr0 = ccif.daddr[1];
	   end
      end
   end // always_comb begin
   //next state logic
   always_comb begin
      nstate = state;
      case(state)
	idle: begin
	   if(ccif.ccwrite[0] || ccif.ccwrite[1]) begin
	      nstate = inv;
	   end
	   else if((ccif.cctrans[0] && ccif.dREN[0]) || (ccif.cctrans[1] && ccif.dREN[1]) && !ccif.dWEN[0] && !ccif.dWEN[1]) begin
	      nstate = snoop;
	   end 
	end // case: idle
	snoop: begin
	   if(ccif.dWEN[sdc] && ccif.cctrans[sdc]) begin
	      nstate = ctc1;
	   end
	   else if(!ccif.dWEN[sdc] && ccif.cctrans[sdc])begin
	      nstate = mtc1;
	   end
	end
	inv: begin
	   if(!(ccif.ccwrite[0] || ccif.ccwrite[1])) begin
	      nstate = idle;
	   end
	end
	mtc1: begin
	   if(ccif.ramstate == ACCESS) begin
	      nstate = mtc2;
	   end
	end
	mtc2: begin
	   if(ccif.ramstate == ACCESS) begin
	      nstate = idle;
	   end
	end
	ctc1: begin
	   if(ccif.ramstate == ACCESS) begin
	      nstate = ctc2;
	   end
	end
	ctc2: begin
	   if(ccif.ramstate == ACCESS) begin
	      nstate = idle;
	   end
	end
      endcase // case (state)
   end // always_comb begin
   
   //output logic
   always_comb begin
      ccif.iwait = 2'b11;
      ccif.dwait = 2'b11;
      ccif.dload = {ccif.ramload, ccif.ramload};
      ccif.ccwait = 2'b00;
      ccif.ccinv = 2'b00;
      ccif.ccsnoopaddr[0] = ccif.daddr[1];
      ccif.ccsnoopaddr[1] = ccif.daddr[0];
      ccif.ramstore = 32'hBAD1BAD1;
      ccif.ramaddr = 32'b0;
      ccif.ramWEN = 1'b0;
      ccif.ramREN = 1'b0;
      case(state)
	idle: begin
	   if(ccif.dWEN[0]) begin
	      ccif.ramWEN = ccif.dWEN[0];
	      ccif.ramaddr = ccif.daddr[0];
	      ccif.ramstore = ccif.dstore[0];
	      ccif.dwait[0] = (ccif.ramstate == ACCESS) ? 1'b0: 1'b1;
	   end
	   else if(ccif.dWEN[1]) begin
	      ccif.ramWEN = ccif.dWEN[1];
	      ccif.ramaddr = ccif.daddr[1];
	      ccif.ramstore = ccif.dstore[1];
	      ccif.dwait[1] = (ccif.ramstate == ACCESS) ? 1'b0: 1'b1;
	   end
	   else if(ccif.iREN[0] || ccif.iREN[1]) begin
	      ccif.ramaddr = ccif.iaddr[iserv];
	      ccif.ramREN = ccif.iREN[iserv];
	      ccif.iwait[iserv] = (ccif.ramstate == ACCESS) ? 1'b0 : 1'b1;
	   end
	end
	inv: begin
	   if (rdc && !sdc) begin
	      ccif.ccinv[sdc] = 1'b1;
	      ccif.ccsnoopaddr[sdc] = invaddr0;
	   end
	   if (!rdc && sdc) begin
	      ccif.ccinv[sdc] = 1'b1;
	      ccif.ccsnoopaddr[sdc] = invaddr1;
	   end
	   if (rdc && sdc) begin
	      ccif.ccinv[0] = 1'b1;
	      ccif.ccsnoopaddr[0] = invaddr0;
	      ccif.ccinv[1] = 1'b1;
	      ccif.ccsnoopaddr[1] = invaddr1;
	   end
	end // case: idle
	snoop: begin
	   ccif.ccwait[sdc] = 1'b1;
	end
	mtc1: begin
	   ccif.ccwait[sdc] = 1'b1;
	   ccif.ramREN = 1'b1;
	   ccif.ramaddr = ccif.daddr[rdc];
	   ccif.dwait[rdc] = (ccif.ramstate == ACCESS) ? 1'b0 : 1'b1;
	end
	mtc2: begin
	   ccif.ccwait[sdc] = (ccif.ramstate == ACCESS) ? 1'b0: 1'b1;
	   ccif.ramREN = 1'b1;
	   ccif.ramaddr = ccif.daddr[rdc];
	   ccif.dwait[rdc] = (ccif.ramstate == ACCESS) ? 1'b0 : 1'b1;
	end
	ctc1: begin
	   ccif.ccwait[sdc] = 1'b1;
	   ccif.ramWEN = 1'b1;
	   ccif.ramaddr = ccif.daddr[sdc];
	   ccif.ramstore = ccif.dstore[sdc];
	   ccif.dload[rdc] = ccif.dstore[sdc];
	   ccif.dwait[rdc] = (ccif.ramstate == ACCESS) ? 1'b0 : 1'b1;
	   ccif.dwait[sdc] = (ccif.ramstate == ACCESS) ? 1'b0 : 1'b1;
	end
	ctc2: begin
	   ccif.ccwait[sdc] = (ccif.ramstate == ACCESS) ? 1'b0: 1'b1;
	   ccif.ramWEN = 1'b1;
	   ccif.ramaddr = ccif.daddr[sdc];
	   ccif.ramstore = ccif.dstore[sdc];
	   ccif.dload[rdc] = ccif.dstore[sdc];
	   ccif.dwait[rdc] = (ccif.ramstate == ACCESS) ? 1'b0 : 1'b1;
	   ccif.dwait[sdc] = (ccif.ramstate == ACCESS) ? 1'b0 : 1'b1;
	end
      endcase // case (state)
   end
   
endmodule
