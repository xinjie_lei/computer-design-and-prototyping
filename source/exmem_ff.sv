/*
 Xinjie Lei
 lei13@purdue.edu
 
 EX/MEM register
 */


`include "ifid_if.vh"
`include "cpu_types_pkg.vh"

module exmem_ff
  import cpu_types_pkg::*;
   (
    input logic CLK,
    input logic nRST,
    exmem_if.exmem exmem
    );
   
   always_ff @ (posedge CLK, negedge nRST)begin
      if(!nRST) begin
	 exmem.npc <= 32'b0;
	 exmem.result <= 32'b0;
	 exmem.dmemstore <= 32'b0;
	 exmem.rdat1 <= 32'b0;
	 exmem.Rd <= 5'b0;
	 exmem.RegSrc <= 2'b0;
	 exmem.dmemREN <= 1'b0;
	 exmem.dmemWEN <= 1'b0;
	 exmem.RegWEN <= 1'b0;
	 exmem.Halt <= 1'b0;
	 exmem.exmeminstru <= 32'b0;
	 exmem.Over <= 1'b0;
	 exmem.Atomic <= 1'b0;	 
      end
      else if (exmem.WEN)begin
	 exmem.npc <= exmem.npc_in;
	 exmem.result <= exmem.result_in;
	 exmem.dmemstore <= exmem.dmemstore_in;
	 exmem.rdat1 <= exmem.rdat1_in;	
	 exmem.Rd <= exmem.Rd_in;
	 exmem.Atomic <= exmem.Atomic_in;
	 
	 exmem.exmeminstru <= exmem.exmeminstru_in;
	 if(exmem.Flush) begin
	    exmem.RegSrc <= 2'b0;
	    exmem.dmemREN <= 1'b0;
	    exmem.dmemWEN <= 1'b0;
	    exmem.RegWEN <= 1'b0;
	    exmem.Halt <= 1'b0;
	    exmem.Over <= 1'b0;
	    exmem.Atomic <= 1'b0;
	    exmem.exmeminstru <= 32'b0;	    
      	 end // if (exmem.Flush)
	 else begin
	    exmem.RegSrc <= exmem.RegSrc_in;	   
	    exmem.dmemREN <= exmem.dmemREN_in;
	    exmem.dmemWEN <= exmem.dmemWEN_in;
	    exmem.RegWEN <= exmem.RegWEN_in;	  
	    exmem.Halt <= exmem.Halt_in;
	    exmem.Over <= exmem.Over_in;
	    exmem.Atomic <= exmem.Atomic_in;
	    
	 end
      end	 
   end

endmodule // exmem register
