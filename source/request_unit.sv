/*
 Xinjie Lei
 lei13@purdue.edu
 
 request unit generates I-Request and D-Access
 */
`include "request_unit_if.vh"
module request_unit (
		     input logic CLK, nRST,
		     request_unit_if ruif
		     );
   logic 			 n_dmemREN;
   logic 			 n_dmemWEN;
   logic 			 n_imemREN;
   
   always_ff @(posedge CLK, negedge nRST) begin
      if(!nRST) begin
	 ruif.imemREN <= 1'b0;
	 ruif.dmemREN <= 1'b0;
	 ruif.dmemWEN <= 1'b0;
      end 
      else begin
	 ruif.imemREN <= n_imemREN;
	 ruif.dmemREN <= n_dmemREN;
	 ruif.dmemWEN <= n_dmemWEN;
      end
   end
   
   always_comb begin
      n_dmemREN = ruif.dmemREN;
      n_dmemWEN = ruif.dmemWEN;
      n_imemREN = 1'b1;
      if(ruif.dhit) begin
	 n_dmemREN = 1'b0;
	 n_dmemWEN = 1'b0;
      end
      if(ruif.memREN && !ruif.dhit) begin
	 n_dmemREN = 1'b1;
      end
      if(ruif.memWEN && !ruif.dhit) begin
	 n_dmemWEN = 1'b1;
      end
      if(ruif.halt) begin
	 n_imemREN = 1'b0;
      end
      
   end

endmodule
