/*
 Xinjie Lei
 lei13@purdue.edu
 
 ID/EX register
 */

`include "branch_target_buffer_if.vh"
`include "cpu_types_pkg.vh"

module branch_target_buffer
  import cpu_types_pkg::*;
  (
   input CLK,
   input nRST,
   branch_target_buffer_if.btb btbif
   );
   
   typedef enum logic [1:0] {sntaken, wntaken, wtaken, staken} stateType;
   stateType prdt[0:3], nprdt[0:3];
   logic [64:0] btb[0:3];
   
   always_ff @(posedge CLK, negedge nRST) begin
      if(!nRST) begin
	 btb[0] <= 64'bx;
	 btb[1] <= 64'bx;
	 btb[2] <= 64'bx;
	 btb[3] <= 64'bx;
      end
      else begin
	 if(!btbif.ishit && btbif.istaken && !btb[btbif.widx][64]) begin	 
	    btb[btbif.widx] = {1'b1, btbif.baddr, btbif.bta};
	 end
	 if(nprdt[btbif.widx] == sntaken) begin
	    btb[btbif.widx][64] = 1'b0;
	 end     
      end // else: !if(!nRST)
   end
   
   //check hit
   always_comb begin
      btbif.hit = 1'b0;
      btbif.br = 1'b0;
      btbif.taddr = {32{1'bx}};
     if(btbif.pc == btb[btbif.ridx][63:32] && btb[btbif.ridx][64]) begin
	btbif.hit = 1'b1;
	btbif.br = prdt[btbif.ridx][1];
	btbif.taddr = btb[btbif.ridx][31:0];
     end
   end // always_comb begin
  
   //2bit predictor
   always_ff @(posedge CLK, negedge nRST) begin
      if(!nRST) begin
	 prdt[0] <= sntaken;
	 prdt[1] <= sntaken;
	 prdt[2] <= sntaken;
	 prdt[3] <= sntaken;
      end
      else begin
	 prdt[btbif.widx] <= nprdt[btbif.widx];
      end
   end
    
   always_comb begin
      nprdt[btbif.widx] = prdt[btbif.widx];
      if(btbif.isbranch)begin
	 case(prdt[btbif.widx])
	   sntaken: begin
	      if(btbif.istaken) begin
		 nprdt[btbif.widx] = wntaken;
	      end
	   end
	   wntaken: begin
	      if(btbif.istaken) begin
		 nprdt[btbif.widx] = staken;
	      end
	      else begin
		 nprdt[btbif.widx] = sntaken;
	      end
	   end
	   wtaken: begin
	      if(btbif.istaken) begin
		 nprdt[btbif.widx] = staken;
	      end
	      else begin
		 nprdt[btbif.widx] = sntaken;
	      end
	   end
	   staken : begin
	      if(!btbif.istaken) begin
		 nprdt[btbif.widx] = wtaken;
	      end
	   end
	 endcase // case (prdt[widx])
      end
   end // always_comb begin
   
endmodule // register_file
