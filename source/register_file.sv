`include "register_file_if.vh"
`include "cpu_types_pkg.vh"

module register_file
  import cpu_types_pkg::*;
  (
   input logic CLK,
   input logic nRST,
   register_file_if.rf rfif
   );
   
   word_t [31:0]rf;
   
   assign rfif.rdat1 = rf[rfif.rsel1];
   assign rfif.rdat2 = rf[rfif.rsel2];
   
   always_ff @ (negedge CLK, negedge nRST)begin
      if(!nRST) begin
	 rf[31:0] = '{default:0};
      end
      else if (rfif.WEN && (rfif.wsel != 5'b0))begin
	 rf[rfif.wsel] = rfif.wdat;
      end
   end

endmodule // register_file
