/*
 Xinjie Lei
 lei13@purdue.edu
 
 this block holds the icache
 */
`include "datapath_cache_if.vh"
`include "cache_control_if.vh"
`include "cpu_types_pkg.vh"

module icache
  import cpu_types_pkg::*;
   (
    input logic CLK, nRST,
    datapath_cache_if.icache dpif,
    cache_control_if ccif
    );
   parameter CPUID = 0;
   
   typedef struct packed {
		  logic [ITAG_W - 1: 0] tag;
		  word_t instruc;
		  logic valid;
		  }block_t;

block_t [15:0] blk;
logic 	hit;
icachef_t fr;

typedef enum logic {idle, update} stateType;
stateType state, nstate;

assign fr.idx = dpif.imemaddr[5:2];
assign fr.tag = dpif.imemaddr[31:6];

assign hit = (fr.tag == blk[fr.idx].tag) && blk[fr.idx].valid;
 
   //next state logic
   always_comb begin
      nstate = state;
      case(state)
	idle: begin
	   if(dpif.imemREN && !hit) begin
	      nstate = update;
	   end
	   else begin
	      nstate = idle;
	   end
	end
	update: begin
	   if(!ccif.iwait[CPUID]) begin
	      nstate = idle;
	   end
	   else begin
	      nstate = update;
	   end
	end
      endcase
   end // always_comb begin
   
   //reg
   always_ff @ (posedge CLK, negedge nRST) begin
      if (!nRST) begin
	 state <= idle;
      end
      else  begin
	 state <= nstate;
      end
   end // always_ff @ (posedge CLK, negedge nRST)
   
   //update cache
   always_ff @(posedge CLK, negedge nRST) begin
      if(!nRST) begin
	 blk <= '{default:0};
      end
      else begin
   	 if (state == update) begin
	    blk[fr.idx].tag = fr.tag;
	    blk[fr.idx].instruc = ccif.iload[CPUID];
	    blk[fr.idx].valid = 1'b1;
	 end
      end
   end // always_ff @ (posedge CLK, negedge nRST)
   
   //output logic
   always_comb begin
      dpif.ihit = 1'b0;
      dpif.imemload = 32'h0;
      ccif.iREN[CPUID] = 1'b0;
      ccif.iaddr[CPUID] = 32'b0;
      case(state)
	idle: begin
	   if(hit) begin
	      dpif.ihit = 1'b1;
	      dpif.imemload = blk[fr.idx].instruc;
	   end
	end
	update: begin
	   ccif.iREN[CPUID] = 1'b1;
	   ccif.iaddr[CPUID] = dpif.imemaddr;
	end
      endcase
   end // always_comb begin
   
endmodule // caches
