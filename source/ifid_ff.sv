/*
 Xinjie Lei
 lei13@purdue.edu
 
 IF/ID register
 */

`include "ifid_if.vh"
`include "cpu_types_pkg.vh"

module ifid_ff
  import cpu_types_pkg::*;
   (
    input logic CLK,
    input logic nRST,
    ifid_if.ifid ifid
    );

   always_ff @ (posedge CLK, negedge nRST)begin
      if(!nRST) begin
	 ifid.npc <= 32'b0;
	 ifid.instruc <= 32'b0;
	 ifid.baddr <= 32'b0;
	 ifid.isbr <= 1'b0;
	 ifid.ishit <= 1'b0;
      end
      else if (ifid.WEN)begin
	 ifid.npc <= ifid.npc_in;	
	 ifid.baddr <= ifid.baddr_in;	 
	 if(ifid.Flush) begin
	    ifid.instruc <= 32'b0;
	    ifid.isbr <= 1'b0;
	    ifid.ishit <= 1'b0;
	 end
	 else begin
	    ifid.instruc <= ifid.instruc_in;
	    ifid.isbr <= ifid.isbr_in;
	    ifid.ishit <= ifid.ishit_in;
	 end
      end
   end

endmodule // IF/ID register